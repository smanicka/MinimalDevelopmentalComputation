# A regenerative network with local controllers.
# The system consists of an 'outer' network that regenerates and preserves patterns, with each cell in this outer network
# consisting of an 'inner' network that helps regenerate the weights of the outer cell that it resides in.
# The inner network is referred to as the 'local' controller; it's the same network (same connectivity, weights and bias)
# that resides in every outer cell.

# This version makes use of retain_grad()

import torch
import numpy as np
import argparse
from Sensitivity import Sensitivity
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.animation import FuncAnimation
import seaborn as sns
import os

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--Mode', type=str, default='Learn')  # Options: Simulate, Learn, LearnPhase1Only
parser.add_argument('--SimulateType', type=str, default='Default')  # Options: Regenerate
parser.add_argument('--InitType', type=str, default='Default')  # Options: HomogeneousLowertau, HomogeneousUppertau, Random
parser.add_argument('--PlotType', type=str, default='Default')  # Options: StateOuterTimeSeries, BiasOuterTimeSeries
parser.add_argument('--AnimateType', type=str, default='Default')  # Options: StateOuterTimeSeries, BiasOuterTimeSeries
parser.add_argument('--SensitivityType', type=str, default='Default')  # Options: FixedParam*, FixedVar*
parser.add_argument('--Stochastic', action='store_true')
parser.add_argument('--numCells', type=int, default=10)
parser.add_argument('--numInitStates', type=int, default=1)
parser.add_argument('--SimTimeSteps', type=int, default=2000)
parser.add_argument('--NumSelectedTimeSteps', type=str, default='Default')  # '0' for testing
parser.add_argument('--EvaluationProp', type=float, default=0.15)
parser.add_argument('--FileNumber', type=int, default=0)
parser.add_argument('--SampleSet', type=int, default=0)
parser.add_argument('--Verbose', action='store_true')
args = parser.parse_args()
Mode = args.Mode
SimulateType = args.SimulateType
InitType = args.InitType
PlotType = args.PlotType
AnimateType = args.AnimateType
SensitivityType = args.SensitivityType
Stochastic = args.Stochastic
numCells = args.numCells
numInitStates = args.numInitStates
SimTimeSteps = args.SimTimeSteps
NumSelectedTimeSteps = args.NumSelectedTimeSteps
if NumSelectedTimeSteps == 'Default':
    NumSelectedTimeSteps = SimTimeSteps - 1
else:
    NumSelectedTimeSteps = int(NumSelectedTimeSteps)
EvaluationProp = args.EvaluationProp
FileNumber = args.FileNumber
SampleSet = args.SampleSet
Verbose = args.Verbose

def computeLatticeAdjacencyMatrix(LatticeDims):
    numrows = LatticeDims[0]
    numcols = LatticeDims[1]
    rowIndices = np.tile(np.arange(0, numrows), numcols)
    colIndices = np.repeat(np.arange(0, numcols), numrows)
    numCellsLayer = numrows * numcols
    cellIndices = np.arange(numCellsLayer)
    AdjMatrix = np.zeros((numCellsLayer, numCellsLayer), dtype=np.int8)
    for cell in cellIndices:
        r = rowIndices[cell]
        c = colIndices[cell]
        rowNeighbors = cellIndices[(rowIndices == r) & (np.abs(colIndices - c) == 1)]
        colNeighbors = cellIndices[(colIndices == c) & (np.abs(rowIndices - r) == 1)]
        AdjMatrix[cell, rowNeighbors] = 1
        AdjMatrix[cell, colNeighbors] = 1
    return (AdjMatrix)

# Define Weight and bias tensors from the above for simulation
def InstantiateSimulationParametersConstants(numSamples):
    # Parameters
    # Controller 1
    AdjInnerCore1 = AdjInner1.clone()
    start1 = numLayer1NodesController1In
    end1 = start1 + numLayer1NodesController1Bidir + numLayer2NodesController1
    start2 = numLayer1NodesController1Out
    end2 = start2 + numLayer1NodesController1Bidir + numLayer2NodesController1
    AdjInnerCore1[start1:end1, start2:end2] = torch.triu(AdjInnerCore1[start1:end1, start2:end2])
    W_inner1 = AdjInnerCore1.clone()
    x,y = np.where(W_inner1==1)
    W_inner1[x,y] = ParWeightsInner1.clone()
    W_inner1[start1:end1, start2:end2] = W_inner1[start1:end1, start2:end2] + W_inner1[start1:end1, start2:end2].t()
    B_inner1 = ParBiasInner1.clone().view(-1,1)
    # Controller 2
    AdjInnerCore2 = AdjInner2.clone()
    start1 = numLayer1NodesController2In
    end1 = start1 + numLayer1NodesController2Bidir + numLayer2NodesController2
    start2 = numLayer1NodesController2Out
    end2 = start2 + numLayer1NodesController2Bidir + numLayer2NodesController2
    AdjInnerCore2[start1:end1, start2:end2] = torch.triu(AdjInnerCore2[start1:end1, start2:end2])
    W_inner2 = AdjInnerCore2.clone()
    x, y = np.where(W_inner2 == 1)
    W_inner2[x, y] = ParWeightsInner2.clone()
    W_inner2[start1:end1, start2:end2] = W_inner2[start1:end1, start2:end2] + W_inner2[start1:end1, start2:end2].t()
    B_inner2 = ParBiasInner2.clone().view(-1, 1)
    if CellType == 'HeterogeneousCellType':
        B_outer = ParBiasOuter.clone().view(-1,1)
    elif CellType == 'HomogeneousCellType':
        B_outer = ParBiasOuter.clone().repeat(numNodesOuter, 1)
    elif CellType == 'DynamicCellType':
        B_outer = (torch.ones(numNodesOuter*numSamples) * 0.0).view(numSamples,numNodesOuter,1)  # outer bias is a variable in this case; initial values set to 0.0
    # # Constants (independent of learnable parameters)
    # StateInnerWeightMask = torch.ones(numNodesInner,numNodesOuter)
    # StateInnerWeightMask[0,0] = 0
    # StateInnerWeightMask[(numWeightsPerOuterCell-1),-1] = 0
    WeightsOuterMax = ParWeightsOuterMax.clone()
    return(W_inner1,B_inner1,W_inner2,B_inner2,B_outer,WeightsOuterMax)

def Evaluate(observed, target):
    [allStateOuter, allStateOuterTimeConstants, allStateInnerTimeConstants] = observed
    [TargetPattern, TargetStateOuterTimeConstants, TargetStateInnerTimeConstants] = target
    initEvalTimeStep = int(EvaluationProp*SimTimeSteps)
    obs1 = allStateOuter[:,:,-initEvalTimeStep:]
    distance1 = (obs1 - TargetPattern.unsqueeze(2)).pow(2).mean()  # shapes: obs = (numSamples,numNodesOuter,SimTimeSteps+1); target = (numNodesOuter,1)
    obs2 = allStateOuterTimeConstants[-initEvalTimeStep:]
    obs2 = obs2 / 2  # scale it so that the distances are comparable and no one distance dominates
    distance2 = (obs2 - TargetStateOuterTimeConstants.unsqueeze(0)).pow(2).mean()
    obs3 = allStateInnerTimeConstants[-initEvalTimeStep:]
    obs3 = obs3 / 2  # scale it so that the distances are comparable and no one distance dominates
    distance3 = (obs3 - TargetStateInnerTimeConstants.unsqueeze(0)).pow(2).mean()
    # distance = ((2/3) * distance1) + ((1/3) * (distance2 + distance3))
    distance = (distance1 + distance2 + distance3) / 3
    return(distance)

def Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                W_outer,B_outer,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,numSamples,
                W_inner1,B_inner1,W_inner2,B_inner2,WeightsOuterMax,Phase='Default'):
    # all* variables as lists rather than tensors
    allStateOuter, allStateInnerOut1, allStateInnerOut2, allStateInnerIn1, allStateInnerIn2, allWeightOuter, allBiasOuter, \
        allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2, \
        allStateOuterSigmoid, allStateInnerOut1Sigmoid = \
        [], [], [], [], [], [], [], [], [], [], [], []
    x, y = np.where(torch.triu(AdjOuter,1)==1)  # does not include self-loops
    WeightsOuter = W_outer[:,x,y].view(numWeightsOuterNonSelfEdges,numSamples)  # ordering is correct
    WeightsOuterSelfLoops = torch.diagonal(W_outer,offset=0,dim1=1,dim2=2).t().view(numWeightsOuterSelfLoops, numSamples)
    WeightsOuterIncSelfLoops = torch.cat((WeightsOuter,WeightsOuterSelfLoops),dim=0)  # shape = (numWeightsOuter,numSamples)
    # initialize sigmoid output variables so their gradients can be tracked
    StateOuterNextDtSigmoid = torch.zeros(StateOuter.shape)
    StateInnerOutSigmoid1 = torch.zeros(StateInnerOut1.shape)
    # set gradient requirements
    if (Mode=='Sensitivity') and (Phase == 'Default'):
        StateOuter.requires_grad = True
        StateInnerOut1.requires_grad = True
        StateInnerOut2.requires_grad = True
        # StateOuterNextDtSigmoid.requires_grad = True
        # StateInnerOutSigmoid1.requires_grad = True
        torch.set_grad_enabled(True)
    for s in range(SimTimeSteps):
        # store tensors
        allStateOuter.append(StateOuter)
        allStateInnerOut1.append(StateInnerOut1)
        allStateInnerOut2.append(StateInnerOut2)
        allStateInnerIn1.append(StateInnerIn1)
        allStateInnerIn2.append(StateInnerIn2)
        allWeightOuter.append(WeightsOuterIncSelfLoops)
        allBiasOuter.append(B_outer)
        allStateOuterTimeConstants.append(StateOuter_timeConstants)
        allStateInnerTimeConstants1.append(StateInner_timeConstants1)
        allStateInnerTimeConstants2.append(StateInner_timeConstants2)
        # allStateOuterSigmoid.append(StateOuterNextDtSigmoid)
        # allStateInnerOut1Sigmoid.append(StateInnerOutSigmoid1)
        if Mode == 'Sensitivity':
            allStateInnerOut1[-1].retain_grad()
            allStateInnerOut2[-1].retain_grad()
            allStateOuter[-1].retain_grad()
            # allStateOuterSigmoid[-1].retain_grad()
            # allStateInnerOut1Sigmoid[-1].retain_grad()
        if SimulateType != 'SingleCell':  # "external" inputs to a single cell are clamped
            StateOuterNextDtSigmoid = torch.sigmoid(StateOuter - B_outer)
            StateOuterNextDtSigmoidWeighted = torch.bmm(W_outer,StateOuterNextDtSigmoid)
            StateOuterNextDt = -StateOuter + StateOuterNextDtSigmoidWeighted
            StateOuter = StateOuter + (StateOuterNextDt * timestep)
            if Stochastic:
                MeanStateOuter = StateOuter.mean().abs().data
                StateOuter = StateOuter + torch.normal(mean=0.0, std=torch.FloatTensor([0.05 * MeanStateOuter]))
            StateOuter = StateOuter / StateOuter_timeConstants.unsqueeze(2)
        # Controller 1
        StateInnerOutSigmoid1 = torch.sigmoid(StateInnerOut1 - B_inner1)
        StateInnerInNextSigmoidWeighted1 = torch.matmul(W_inner1, StateInnerOutSigmoid1)  # NOTE: Possible source of memory leak
        StateInnerInNextDt1 = -StateInnerIn1 + StateInnerInNextSigmoidWeighted1
        StateInnerIn1 = StateInnerIn1 + (StateInnerInNextDt1 * timestep)
        StateInnerIn1 = StateInnerIn1 / StateInner_timeConstants1
        # Note that the outer bias will be contained within [-1,1], which is fine as long as the outer states are within the same range and transformed through a standard sigmoid with slope 1 (hard-coding)
        # Order of variables (rows) in StaterInnerIn1: left weight, bias, self weight, right weight and the rest of the controller nodes
        if CellType == 'DynamicCellType':
            B_outer = StateInnerIn1[:,1,:].view(numSamples,numNodesOuter,1)  # outer bias index = 1 (hard-coding)
        WeightsOuter = StateInnerIn1[:,[0,numWeightsPerOuterCell],:]  # outer weight indices = [0, numWeightsPerOuterCell] (hard-coding)
        WeightsOuter = WeightsOuter.view(numSamples,1,-1)
        if SimulateType == 'SingleCell':
            WeightsOuterTrim = WeightsOuter[:,0,:]
        else:  # SimulateType = 'Default'
            WeightsOuterTrim = WeightsOuter[:, :, 1:-1].view(numSamples,2,-1)  # trim the trailing (boundary) zeros for now; num outer weights per cell for chain = 2 (hard-coding)
            WeightsOuterTrim = WeightsOuterTrim.mean(dim=1)  # column-wise sum
            WeightsOuterTrim = WeightsOuterTrim * WeightsOuterMax
            W_outer = torch.triu(AdjOuter,1)  # does not include diagonal
            W_outer = W_outer.repeat(numSamples,1,1)  # shape = (numSamples,numNodesOuter,numNodesOuter)
            x,y = np.where(W_outer[0,:,:]==1)
            W_outer[:,x,y] = torch.FloatTensor(WeightsOuterTrim)  # ordering is correct
            W_outer = W_outer + W_outer.transpose(1,2)
        # include self-loop weights
        WeightsSelfLoop = StateInnerIn1[:,2,:].view(numSamples, numNodesOuter)  # self-loop weight index = 2 (hard-coding)
        WeightsSelfLoop = WeightsSelfLoop * WeightsOuterMax
        W_outer_diag = torch.diag_embed(WeightsSelfLoop)  # ordering is correct: every row of WeightsSelfLoop is a diagonal W_outer_diag w.r.t. dims 1 and 2
        W_outer = W_outer + W_outer_diag
        # NOTE: StateInnerOut and StateIStateInnerIn[:,[0,numWeightsPerOuterCell],:] have the states of Layer2NodesController in common (because layer2 nodes are both 'in' and 'out')
        StateInnerOut1 = StateInnerIn1[:,numLayer1NodesController1In:,]  # shape = (numSamples,numLayer1NodesController1Bidir+numLayer2NodesController1,numNodesOuter)
        StateInnerOut1 = torch.cat((StateOuter.transpose(1,2),StateInnerOut1),dim=1)  # append StateOuter since it's an 'out' node
        WeightsOuterIncSelfLoops = torch.cat((WeightsOuterTrim,WeightsSelfLoop),dim=1)
        WeightsOuterIncSelfLoops = WeightsOuterIncSelfLoops.transpose(0,1)
        # Controller 2
        # Update StateInner2
        StateInnerOutSigmoid2 = torch.sigmoid(StateInnerOut2 - B_inner2)
        StateInnerInNextSigmoidWeighted2 = torch.matmul(W_inner2, StateInnerOutSigmoid2)  # NOTE: Possible source of memory leak
        StateInnerInNextDt2 = -StateInnerIn2 + StateInnerInNextSigmoidWeighted2
        StateInnerIn2 = StateInnerIn2 + (StateInnerInNextDt2 * timestep)
        StateInnerIn2 = StateInnerIn2 / StateInner_timeConstants2
        # Compute timeconstant change
        # Order of variables (rows) in StaterInnerIn2: left tau, self tau, right tau and the rest of the controller nodes
        timeConstantTimeConstantsLeftRightDt = StateInnerIn2[:,[0,2],:]  # left and right timeconstants indices = 0,2 (hard-coding)
        timeConstantTimeConstantsLeftRightDt = torch.cat((torch.zeros(numSamples,2,1),timeConstantTimeConstantsLeftRightDt,torch.zeros(numSamples,2,1)),2).view(numSamples,1,-1)
        timeConstantTimeConstantsLeftRightDt = timeConstantTimeConstantsLeftRightDt[:,:,2:-2].view(numSamples,2,-1) # 2:-2 because the timeconstant of a cell is determined by cells on the left and right that are not adjacent cells (unlike for outer weights)
        timeConstantTimeConstantsSelfDt = StateInnerIn2[:,[1],:]  # self timeconstants indices = 1 (hard-coding)
        timeConstantTimeConstantsDt = torch.cat((timeConstantTimeConstantsLeftRightDt, timeConstantTimeConstantsSelfDt), dim=1)
        timeConstantTimeConstantsDt = timeConstantTimeConstantsDt.mean(dim=1)  # column-wise sum; shape = (numSamples,numNodesOuter)
        # Update timeconstants: this is where the 'timecontant change' is broadcasted to all the other timeconstants; since the timeconstant of timeconstant is always 1 they won't change
        StateOuter_timeConstants = StateOuter_timeConstants + (timeConstantTimeConstantsDt * timestep)  # StateOuter shape = (numSamples,numNodesOuter)
        StateInner_timeConstants1 = StateInner_timeConstants1 + (timeConstantTimeConstantsDt.unsqueeze(1) * timestep)  #StateInner1 shape = (numSamples,numInNodesController1,numNodesOuter)
        StateInner_timeConstants2 = StateInner_timeConstants2 + (timeConstantTimeConstantsDt.unsqueeze(1) * timestep)  #StateInner2 shape = (numSamples,numInNodesController2,numNodesOuter)
        # Compute StateInnerOut2
        StateInnerOut2 = StateInnerIn2[:,numLayer1NodesController2In:,]  # shape = (numSamples,numLayer1NodesController2Bidir+numLayer2NodesController2,numNodesOuter)
        TimeConstants = StateInner_timeConstants2[:,0,:].unsqueeze(1)  # shape = (numSamples,1,numNodesOuter)
        StateInnerOut2 = torch.cat((TimeConstants, StateInnerOut2), dim=1)  # append tau since it's an 'out' node
        StateInnerOut2 = torch.cat((StateOuter.transpose(1,2),StateInnerOut2),dim=1)  # append StateOuter since it's an 'out' node
    return(allStateOuter,allWeightOuter,allBiasOuter,allStateInnerOut1,allStateInnerOut2,allStateInnerIn1,allStateInnerIn2,
           StateOuter,StateInnerIn1,StateInnerIn2,StateInnerOut1,StateInnerOut2,B_outer,W_outer,allStateOuterTimeConstants,
           allStateInnerTimeConstants1,allStateInnerTimeConstants2,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,
           allStateOuterSigmoid,allStateInnerOut1Sigmoid)

## Specify simulation parameters
# Mode = 'Learn'  # Options: Simulate, Learn
timestep = 0.01
if (Mode == 'Simulate') or (Mode == 'SimulatePhase1Only') or (Mode == 'Sensitivity'): # or (Mode == 'Plot') or (Mode == 'Animate') or (Mode == 'Analyze'):
    MinTotalLoss, BestParams = torch.load('./Data/BestParamsLocal_DynamicSize_MultipleControllers_' + str(FileNumber) + '.dat')
    CellType = 'DynamicCellType'  # this must be saved in BestParams
    if CellType == 'DynamicCellType':
        ParWeightsInner1, ParWeightsInner2, ParBiasInner1, ParBiasInner2, \
        AdjOuter, AdjInner1, AdjInner2, \
        LayeringController1InterDirections, LayeringController2InterDirections, \
        LayeringController1, LayeringController2, ParWeightsOuterMax, \
        ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
    else:
        ParWeightsInner, ParBiasInner, ParBiasOuter, \
        AdjOuter, AdjInner, \
        LayeringControllerInterDirections, LayeringController, ParWeightsOuterMax, \
        ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
    numStopCells = 2
    numNodesOuter = AdjOuter.size()[0]
    # Update AdjOuter if simulating with a different network size
    if SimulateType == 'Rescale':  # assumes a new value of numCells is provided
        numNodesOuter = numCells + numStopCells  # one 'stop' cell at each pole
        LayeringOuter = [numNodesOuter, 1]  # outer layer nodes
        AdjOuter = computeLatticeAdjacencyMatrix(LayeringOuter)  # numpy matrix; self-loops are added separately below
        # Add self-loops to outer cells
        cellIndices = list(range(numNodesOuter))
        AdjOuter[cellIndices, cellIndices] = 1
        AdjOuter = torch.FloatTensor(AdjOuter)
    elif SimulateType == 'SingleCell':  # equivalent to Rescale with numCells=1 but with numStopCells=0
        numStopCells = 0  # valid only for the SingleCell case
        numNodesOuter = 1
        LayeringOuter = [numNodesOuter, 1]  # outer layer nodes
        AdjOuter = computeLatticeAdjacencyMatrix(LayeringOuter)  # numpy matrix; self-loops are added separately below
        # Add self-loops to outer cells
        cellIndices = list(range(numNodesOuter))
        AdjOuter[cellIndices, cellIndices] = 1
        AdjOuter = torch.FloatTensor(AdjOuter)
    numWeightsOuterNonSelfEdges = int(np.sum(np.triu(AdjOuter, 1)))  # edges not including self-loops
    numWeightsOuterSelfLoops = int(np.sum(np.diag(AdjOuter)))  # self-loops only
    numWeightsOuter = numWeightsOuterNonSelfEdges + numWeightsOuterSelfLoops
    if numNodesOuter > 1:
        numWeightsPerOuterCell = int(torch.sum(AdjOuter[1,:]))  # in-degree of cell; same for all cells with indices 1 through (n-2) assuming homogeneous connectivity; Note that cells 0 and (n-1) have a smaller degree than the rest; includes self-loops
    else:
        numWeightsPerOuterCell = 0
    numWeightsPerOuterCellSelfLoop = int(AdjOuter[0,0].item())  # assuming all cells are connected in the same way (e.g., either all cells have self-loops or they don't)
    numWeightsPerOuterCellNonSelfEdges = numWeightsPerOuterCell - numWeightsPerOuterCellSelfLoop
    # Controller1 variables
    numLayer1NodesController1In = len([x for x in LayeringController1InterDirections if x == 'in'])
    numLayer1NodesController1Out = len([x for x in LayeringController1InterDirections if x == 'out'])
    numLayer1NodesController1Bidir = len([x for x in LayeringController1InterDirections if x == 'bidirectional'])
    numLayer1NodesController1 = numLayer1NodesController1In + numLayer1NodesController1Out + numLayer1NodesController1Bidir
    cells_per_layer_ctrl1 = [np.prod(x) for x in LayeringController1]
    numLayer2NodesController1 = cells_per_layer_ctrl1[1]
    numInNodesController1 = numLayer1NodesController1In + numLayer1NodesController1Bidir + numLayer2NodesController1
    numOutNodesController1 = numLayer1NodesController1Out + numLayer1NodesController1Bidir + numLayer2NodesController1
    numAdjInnerCells1 = numLayer1NodesController1 + numLayer2NodesController1
    # Controller2 variables
    numLayer1NodesController2In = len([x for x in LayeringController2InterDirections if x == 'in'])
    numLayer1NodesController2Out = len([x for x in LayeringController2InterDirections if x == 'out'])
    numLayer1NodesController2Bidir = len([x for x in LayeringController2InterDirections if x == 'bidirectional'])
    numLayer1NodesController2 = numLayer1NodesController2In + numLayer1NodesController2Out + numLayer1NodesController2Bidir
    cells_per_layer_ctrl2 = [np.prod(x) for x in LayeringController2]
    numLayer2NodesController2 = cells_per_layer_ctrl2[1]
    numInNodesController2 = numLayer1NodesController2In + numLayer1NodesController2Bidir + numLayer2NodesController2
    numOutNodesController2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir + numLayer2NodesController2
    numAdjInnerCells2 = numLayer1NodesController2 + numLayer2NodesController2
    LearnIters = 1
    targetLowerTimeConstant, targetUpperTimeConstant = 1, 2  # (hard-coding)
    TargetStateOuterTimeConstants = torch.ones(numInitStates,numNodesOuter) * targetLowerTimeConstant
    TargetStateOuterTimeConstants[:,[0,-1]] = targetUpperTimeConstant  # stop cells
    TargetStateInnerTimeConstants = torch.ones(numInitStates,numInNodesController1,numNodesOuter)  # WHY DOES THIS DEPEND ON numInNodesController?
    TargetStateInnerTimeConstants[:,:,[0,-1]] = targetUpperTimeConstant   # stop cells

def MasterAnalyze():

    for learniter in range(LearnIters):

        W_inner1, B_inner1, W_inner2, B_inner2, B_outer, WeightsOuterMax = InstantiateSimulationParametersConstants(numInitStates)  # Note that W_outer (WeightsOuter) is a variable, not a parameter

        ## Phase 1
        # Initial conditions for outer and inner states
        # The same initial outer state regardless of numInitStates since it was found that they hardly influence the outcome
        # Also, numInitStates will dictate the number of initial alive cells (see below)
        # Initial condition of outer state is part of the inner state, since the state of each cell feeds into the cell's "GRN"
        # This state will be referred to as 'StateInnerOut' since it contains all variables with out and bidir links;
        # while the state that contains variables with only in and bidir links will be referred to as 'StateInnerIn'.
        if SimulateType == 'SingleCell':
            StateOuter = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(-1, 1, numInitStates))))  # shape = (numInitStates,numNodesOuter)
        else:  # SimulateType == 'Default'
            if 'Homogeneous' in InitType:  # initial outer state with all 0s
                StateOuter = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(0, 0, numInitStates))))  # shape = (numInitStates,numNodesOuter)
            elif InitType == 'Random':
                StateOuter = torch.FloatTensor(np.random.uniform(-1, 1, numInitStates*(numNodesOuter-numStopCells))).view(numInitStates, numNodesOuter-numStopCells)
                # first sample is the training sample
                StateOuter[0] = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(1, 1, 1))))
            else:  # InitType == 'Default'
                StateOuter = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(1, 1, numInitStates))))  # shape = (numInitStates,numCells)
        if numStopCells == 2:
            StateOuter = torch.cat((torch.tensor([0]*numInitStates).view(-1, 1), StateOuter, torch.tensor([0]*numInitStates).view(-1, 1)), dim=1)  # padding values for stop cells
        if InitType == 'NegativeGradient':  # assumes numInitStates = 1; and numNodesOuter = 12 = numInitStates of SingleCell
            SingleCellStateOuter, SingleCellStateInnerIn1, SingleCellStateInnerIn2, SingleCellStateInnerOut1, SingleCellStateInnerOut2, \
                SingleCellB_outer, SingleCellW_outer, \
                SingleCellStateOuter_timeConstants, SingleCellStateInner_timeConstants1, SingleCellStateInner_timeConstants2 = \
                torch.load('./Data/SingleCellData.dat')
            StateOuter = SingleCellStateOuter.transpose(0,1)  # shape = (1,numNodesOuter,1); assumes numInitStates = 1
            StateInnerIn1 = SingleCellStateInnerIn1.transpose(0,2)
            StateInnerIn2 = SingleCellStateInnerIn2.transpose(0,2)
            StateInnerOut1 = SingleCellStateInnerOut1.transpose(0,2)
            StateInnerOut2 = SingleCellStateInnerOut2.transpose(0,2)
            # # TEST: reverse signs to see if it reverses the sign of the stable states
            # StateInnerIn1 = StateInnerIn1 * -1
            # StateInnerIn2 = StateInnerIn2 * -1
            # StateInnerOut1 = StateInnerOut1 * -1
            # StateInnerOut2 = StateInnerOut2 * -1
            B_outer = SingleCellB_outer.transpose(0,1)
            StateOuter_timeConstants = SingleCellStateOuter_timeConstants.transpose(0,1)
            StateInner_timeConstants1 = SingleCellStateInner_timeConstants1.transpose(0,2)
            StateInner_timeConstants2 = SingleCellStateInner_timeConstants2.transpose(0,2)
            StateOuter_timeConstants = StateOuter_timeConstants/StateOuter_timeConstants
            StateInner_timeConstants1 = StateInner_timeConstants1/StateInner_timeConstants1
            StateInner_timeConstants2 = StateInner_timeConstants2/StateInner_timeConstants2
            W_outer = torch.zeros(numNodesOuter,numNodesOuter)
            for node in range(1,numCells+1):  # assuming numCells = 10
                W_outer[node-1,node] = SingleCellW_outer[0,node]
                W_outer[node+1,node] = SingleCellW_outer[1,node]
                W_outer[node,node] = SingleCellW_outer[2,node]
            W_outer = (torch.triu(W_outer) + torch.tril(W_outer).t()) / 2
            W_outer = W_outer + W_outer.t()
            W_outer = W_outer.unsqueeze(0)  # # shape = (numInitStates,numNodesOuter,numNodesOuter); numInitStates = 1
            NumAliveCells = torch.zeros(numInitStates)
        else:
            StateOuter = StateOuter.unsqueeze(2)  # shape = (numInitStates,numNodesOuter,1) to match with W_outer shape = (numSamples,numNodesOuter,numNodesOuter)
            StateInnerIn1 = torch.zeros(numInitStates,numInNodesController1,numNodesOuter)
            StateInnerIn2 = torch.zeros(numInitStates, numInNodesController2, numNodesOuter)
            StateInnerOut1 = torch.zeros(numInitStates,numOutNodesController1,numNodesOuter)
            StateInnerOut2 = torch.zeros(numInitStates, numOutNodesController2, numNodesOuter)
            StateInnerOut1[:,0,:] = StateOuter[:,:,0].detach()  # clone is not necessary here; detach will work
            StateInnerOut2[:,0,:] = StateOuter[:,:,0].detach()  # clone is not necessary here; detach will work
            # NOTE: StateInnerOut and StateInnerIn have the states of Layer2NodesController in common (because layer2 nodes are both 'in' and 'out')
            # create outer weight matrix from the weight state vector
            W_outer = torch.triu(AdjOuter)
            x,y = np.where(W_outer==1)
            WeightsOuter1Flat = np.zeros(numWeightsOuter)  # initial outer weights are set to 0
            W_outer[x,y] = torch.FloatTensor(WeightsOuter1Flat)
            W_outer = W_outer + W_outer.t()
            W_outer = W_outer.repeat(numInitStates,1,1)  # shape = (numInitStates,numNodesOuter,numNodesOuter)
            # B_outer = B_outer.clone()  # shape = (numNodesOuter,numInitStates)
            StateOuter_timeConstants = torch.ones(numInitStates,numNodesOuter) * targetUpperTimeConstant   # all but the middle cells are dead initially # Not the same values as StateInnerIn1[:,-2:,], as the latter are timeconstant changes, not timeconstants themselves
            StateInner_timeConstants1 = torch.ones(numInitStates,numInNodesController1,numNodesOuter) * targetUpperTimeConstant  # all but the middle cells are dead initially # same values as StateInnerIn1[:,-2:,] initially
            StateInner_timeConstants2 = torch.ones(numInitStates,numInNodesController2,numNodesOuter) * targetUpperTimeConstant  # all but the middle cells are dead initially # same values as StateInnerIn1[:,-2:,] initially
            center = (numNodesOuter / 2) + 0.5  # median cell
            initOffset = 0.5 if numCells % 2 == 0 else 1
            # cutOffsets = np.arange(initOffset, numInitStates + 0.5, 1)  # the length of this array should be equal to numInitStates = halfNumCells
            cutOffsets = initOffset
            NumAliveCells = torch.zeros(numInitStates)
            for sample in range(numInitStates):
                offset = cutOffsets
                if 'Homogeneous' in InitType:  # all cells have the same timeconstant
                    st = 0; nd = numNodesOuter
                elif InitType == 'Random':
                    st, nd = int(center - offset) - 1, int(center + offset)  # 5,7 for offset=0.5
                    if sample > 0:  # first sample is the training sample
                        st -= np.random.randint(0, int(numNodesOuter / 2))
                        nd += np.random.randint(0, int(numNodesOuter / 2))
                else:  # InitType = 'Default'
                    st, nd = int(center - offset) - 1, int(center + offset)
                numAliveCells = nd - st
                NumAliveCells[sample] = numAliveCells
                # only the alive cells have non-zero initial states; all variables except StateOuter are set to 0 regardless
                StateOuter[sample, 0:st, :] = 0
                StateOuter[sample, nd:, :] = 0
                if InitType == 'HomogeneousLowertau':
                    initTau = targetLowerTimeConstant
                elif InitType == 'HomogeneousUppertau':
                    initTau = targetUpperTimeConstant
                elif InitType == 'Random':
                    if sample == 0:  # first sample is the training sample
                        initTau = targetLowerTimeConstant
                    else:
                        initTau = torch.FloatTensor(np.random.uniform(1, 2, numAliveCells))
                else:  # InitType = Default
                    initTau = targetLowerTimeConstant
                StateOuter_timeConstants[sample, st:nd] = initTau  # only the middle cells are alive initially
                StateInner_timeConstants1[sample, :, st:nd] = initTau  # only the middle cells are alive initially
                StateInner_timeConstants2[sample, :, st:nd] = initTau  # only the middle cells are alive initially
        # simulate
        allStateOuter, allWeightOuter, allBiasOuter, allStateInnerOut1, allStateInnerOut2, \
        allStateInnerIn1, allStateInnerIn2, StateOuter, StateInnerIn1, StateInnerIn2, StateInnerOut1, \
        StateInnerOut2, B_outer, W_outer, allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2,\
        StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2, \
        allStateOuterSigmoid, allStateInnerOut1Sigmoid = \
            Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                     W_outer,B_outer,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,numInitStates,
                     W_inner1,B_inner1,W_inner2,B_inner2,WeightsOuterMax,Phase='Default')
        if SimulateType == 'SingleCell':  # save single-cell data
            SingleCellData = [StateOuter,StateInnerIn1,StateInnerIn2,StateInnerOut1,StateInnerOut2,B_outer,allWeightOuter[-1], \
                              StateOuter_timeConstants, StateInner_timeConstants1, StateInner_timeConstants2]
            torch.save(SingleCellData,'./Data/SingleCellData.dat')
        if SimulateType == 'Regenerate':
            # set initial conditions for regeneration phase
            for sample in range(numInitStates):
                offset = cutOffsets
                st, nd = int(center - offset) - 1, int(center + offset)
                # # shift locations of initially alive cells
                # st -= 4; nd+= 4  # 1, 11
                # # TEST: Swap left and right halves, regardless of st and nd (then apply st and nd)
                # for node in range(int(numNodesOuter/2)):
                #     left, right = node, numNodesOuter-1-node
                #     temp = StateOuter[sample, left].clone()
                #     StateOuter[sample, left] = StateOuter[sample, right].clone()
                #     StateOuter[sample, right] = temp.clone()
                #     temp = B_outer[sample, left].clone()
                #     B_outer[sample, left] = B_outer[sample, right].clone()
                #     B_outer[sample, right] = temp.clone()
                #     temp = W_outer[sample, left, left].clone()
                #     W_outer[sample, left, left] = W_outer[sample, right, right].clone()
                #     W_outer[sample, right, right] = temp.clone()
                #     temp = W_outer[sample, left, left+1].clone()
                #     W_outer[sample, left, left+1] = W_outer[sample, right, right-1].clone()
                #     W_outer[sample, right, right-1] = temp.clone()
                #     temp = W_outer[sample, left+1, left].clone()
                #     W_outer[sample, left+1, left] = W_outer[sample, right-1, right].clone()
                #     W_outer[sample, right-1, right] = temp.clone()
                #     temp = StateInnerOut1[sample, :, left].clone()
                #     StateInnerOut1[sample, :, left] = StateInnerOut1[sample, :, right].clone()
                #     StateInnerOut1[sample, :, right] = temp.clone()
                #     temp = StateInnerOut2[sample, :, left].clone()
                #     StateInnerOut2[sample, :, left] = StateInnerOut2[sample, :, right].clone()
                #     StateInnerOut2[sample, :, right] = temp.clone()
                #     temp = StateInnerIn1[sample, :, left].clone()
                #     StateInnerIn1[sample, :, left] = StateInnerIn1[sample, :, right].clone()
                #     StateInnerIn1[sample, :, right] = temp.clone()
                #     temp = StateInnerIn2[sample, :, left].clone()
                #     StateInnerIn2[sample, :, left] = StateInnerIn2[sample, :, right].clone()
                #     StateInnerIn2[sample, :, right] = temp.clone()
                #     temp = StateOuter_timeConstants[sample, left].clone()
                #     StateOuter_timeConstants[sample, left] = StateOuter_timeConstants[sample, right].clone()
                #     StateOuter_timeConstants[sample, right] = temp.clone()
                #     temp = StateInner_timeConstants1[sample, :, left].clone()
                #     StateInner_timeConstants1[sample, :, left] = StateInner_timeConstants1[sample, :, right].clone()
                #     StateInner_timeConstants1[sample, :, right] = temp.clone()
                #     temp = StateInner_timeConstants2[sample, :, left].clone()
                #     StateInner_timeConstants2[sample, :, left] = StateInner_timeConstants2[sample, :, right].clone()
                #     StateInner_timeConstants2[sample, :, right] = temp.clone()
                StateOuter[sample, 0:st, :] = 0
                StateOuter[sample, nd:, :] = 0
                B_outer[sample, 0:st, :] = 0
                B_outer[sample, nd:, :] = 0
                W_outer[sample, 0:st, 0:st] = 0
                W_outer[sample, nd:, nd:] = 0
                StateInnerOut1[sample, :, 0:st] = 0
                StateInnerOut1[sample, :, nd:] = 0
                StateInnerOut2[sample, :, 0:st] = 0
                StateInnerOut2[sample, :, nd:] = 0
                StateInnerIn1[sample, :, 0:st] = 0
                StateInnerIn1[sample, :, nd:] = 0
                StateInnerIn2[sample, :, 0:st] = 0
                StateInnerIn2[sample, :, nd:] = 0
                StateOuter_timeConstants[sample, 0:st] = targetUpperTimeConstant
                StateOuter_timeConstants[sample, nd:] = targetUpperTimeConstant
                StateInner_timeConstants1[sample, :, 0:st] = targetUpperTimeConstant
                StateInner_timeConstants1[sample, :, nd:] = targetUpperTimeConstant
                StateInner_timeConstants2[sample, :, 0:st] = targetUpperTimeConstant
                StateInner_timeConstants2[sample, :, nd:] = targetUpperTimeConstant
            # simulate
            allStateOuterRegen, allWeightOuterRegen, allBiasOuterRegen, allStateInnerOut1Regen, allStateInnerOut2Regen, \
            allStateInnerIn1Regen, allStateInnerIn2Regen, StateOuter, StateInnerIn1, StateInnerIn2, StateInnerOut1, \
            StateInnerOut2, B_outer, W_outer, allStateOuterTimeConstantsRegen, allStateInnerTimeConstants1Regen, allStateInnerTimeConstants2Regen, \
            StateOuter_timeConstants, StateInner_timeConstants1, StateInner_timeConstants2, \
            allStateOuterSigmoidRegen, allStateInnerOut1SigmoidRegen = \
                Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                         W_outer,B_outer,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,numInitStates,
                         W_inner1,B_inner1,W_inner2,B_inner2,WeightsOuterMax,Phase='Regenerate')
            allStateOuter.extend(allStateOuterRegen)
            allWeightOuter.extend(allWeightOuterRegen)
            allBiasOuter.extend(allBiasOuterRegen)
            allStateInnerOut1.extend(allStateInnerOut1Regen)
            allStateInnerOut2.extend(allStateInnerOut2Regen)
            allStateInnerIn1.extend(allStateInnerIn1Regen)
            allStateInnerIn2.extend(allStateInnerIn2Regen)
            allStateOuterTimeConstants.extend(allStateOuterTimeConstantsRegen)
            allStateInnerTimeConstants1.extend(allStateInnerTimeConstants1Regen)
            allStateInnerTimeConstants2.extend(allStateInnerTimeConstants2Regen)
            allStateOuterSigmoid.extend(allStateOuterSigmoidRegen)
            allStateInnerOut1Sigmoid.extend(allStateInnerOut1SigmoidRegen)
        if SimulateType == 'Perturb':
            StateOuter_timeConstants = allStateOuterTimeConstants[-1].clone()
            StateInner1_timeConstants = allStateInnerTimeConstants1[-1].clone()
            StateInner2_timeConstants = allStateInnerTimeConstants2[-1].clone()
            perturbCells = [3]
            # copyCells = [perturbCells[i]-(2*(i+1)) for i in range(len(perturbCells))]
            copyCells = [perturbCells[i]-((2*i)+1) for i in range(len(perturbCells))]
            for sample in range(numInitStates):
                StateOuter[sample, perturbCells] = 100*StateOuter[sample, copyCells].detach()
                StateOuter[sample, (perturbCells[-1]+1):] = 0
                B_outer[sample, (perturbCells[-1]+1):, :] = 0
                W_outer[:, (perturbCells[-1]+1):, (perturbCells[-1]+1):] = 0
                StateInnerOut1[:, :, (perturbCells[-1]+1):] = 0
                StateInnerOut2[:, :, (perturbCells[-1]+1):] = 0
                StateInnerIn1[:, :, (perturbCells[-1]+1):] = 0
                StateInnerIn2[:, :, (perturbCells[-1]+1):] = 0
                StateOuter_timeConstants[sample, (perturbCells[-1]+1):] = targetUpperTimeConstant
                StateInner_timeConstants1[sample, :, (perturbCells[-1]+1):] = targetUpperTimeConstant
                StateInner_timeConstants2[sample, :, (perturbCells[-1]+1):] = targetUpperTimeConstant
            # simulate
            allStateOuterPert, allWeightOuterPert, allBiasOuterPert, allStateInnerOut1Pert, allStateInnerOut2Pert, \
            allStateInnerIn1Pert, allStateInnerIn2Pert, StateOuter, StateInnerIn1, StateInnerIn2, StateInnerOut1, \
            StateInnerOut2, B_outer, W_outer, allStateOuterTimeConstantsPert, allStateInnerTimeConstants1Pert, allStateInnerTimeConstants2Pert, \
            StateOuter_timeConstants, StateInner_timeConstants1, StateInner_timeConstants2 = \
                Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                         W_outer,B_outer,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,numInitStates,
                         W_inner1,B_inner1,W_inner2,B_inner2,WeightsOuterMax)
            allStateOuter.extend(allStateOuterPert)
        # # compute loss
        # observed = [allStateOuter,allStateOuterTimeConstants,allStateInnerTimeConstants1]
        # target = [TargetPattern,TargetStateOuterTimeConstants,TargetStateInnerTimeConstants]
        # Loss = Evaluate(observed,target)
        # if Verbose:
        #     print(Loss.data)
        return (allStateOuter, allWeightOuter, allBiasOuter, allStateInnerOut1, allStateInnerOut2,
                allStateInnerIn1, allStateInnerIn2, StateOuter, StateInnerIn1, StateInnerIn2,
                StateInnerOut1, StateInnerOut2, B_outer, W_outer,
                allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2,
                allStateOuterSigmoid, allStateInnerOut1Sigmoid, NumAliveCells)

def animateHeatmap(data, fname):
    fig = plt.figure()
    im = plt.imshow(data[0], cmap=plt.cm.coolwarm)

    # Make sure to call set_data() in init() and then set_array() in update(); not following this order could result in a blank slate
    def init():
        im.set_data(data[0])
        plt.xlabel('Caused cell', fontsize=18)
        plt.ylabel('Causing cell', fontsize=18)
        return im,  # required only when blit=True

    def update(i):
        im.set_array(data[i])
        return im,  # required only when blit=True

    ani = FuncAnimation(fig, update, frames=np.arange(0, data.shape[0]), init_func=init, blit=True, interval=1, repeat=False)
    Writer = animation.writers['ffmpeg']
    mywriter = Writer(fps=100, metadata=dict(artist='Me'), bitrate=1800)
    ani.save(fname, writer=mywriter)

def animateTimeSeries(data, initdata, fname):
    fig, ax = plt.subplots()
    x = range(1,numNodesOuter+1)
    y = initdata
    target, = ax.plot(x, y, color='red')
    y = np.repeat(0,numNodesOuter)
    line, = ax.plot(x, y, color='black')

    def init():
        plt.xlabel('Cell')
        plt.ylabel('Cell activity (normalized)')
        plt.xlim(1,numNodesOuter)
        plt.ylim(-1.0,1.0)
        ax.set_xticks(list(x))
        ax.set_xticklabels(list(x))
        return target,line,

    def update(i):
        y = data[:,i].view(-1, 1)
        line.set_ydata(y)
        return line,

    ani = FuncAnimation(fig, update, frames=np.arange(0, data.shape[1]), init_func=init, blit=True, interval=1, repeat=False)

    Writer = animation.writers['ffmpeg']
    mywriter = Writer(fps=100, metadata=dict(artist='Me'), bitrate=1800)
    ani.save(fname, writer=mywriter)

    # mywriter = animation.FFMpegWriter(fps=100, extra_args=['-vcodec', 'libx264'])
    # ani.save(fname, writer=mywriter)

def makeHeatmap(heatmap,xlab,ylab,title,savefname):
    fig, _ = plt.subplots()
    sns.heatmap(heatmap, cmap="coolwarm")
    plt.xlabel(xlab, fontsize=18)
    plt.ylabel(ylab, fontsize=18)
    plt.title(title)
    fig.tight_layout()
    plt.savefig('./Data/' + savefname)

if Mode == 'Sensitivity':
    allStateOuter, allWeightOuter, allBiasOuter, allStateInnerOut1, allStateInnerOut2, \
        allStateInnerIn1, allStateInnerIn2, StateOuter, StateInnerIn1, StateInnerIn2, \
        StateInnerOut1, StateInnerOut2, B_outer, W_outer, \
        allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2,\
        allStateOuterSigmoid, allStateInnerOut1Sigmoid, NumAliveCells = MasterAnalyze()
    SimTimeSteps = len(allStateOuter)
    if NumSelectedTimeSteps > 0:
        if 'FixedParam' in SensitivityType:
            EndTimeSteps = SimTimeSteps - 1
            SelectedTimeSteps = np.linspace(2, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
        elif 'FixedVar' in SensitivityType:
            EndTimeSteps = SimTimeSteps - 3  # min causality offset = 2; so, for SimTimeSteps=2000, EndTimeSteps=1999-2=1997
            SelectedTimeSteps = np.linspace(0, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
            # SelectedTimeSteps = np.array([0,1])  # missed out timesteps during the runs on the cluster
    elif NumSelectedTimeSteps < 0:
        if 'FixedParam' in SensitivityType:
            StartTimeStep = SimTimeSteps + NumSelectedTimeSteps
            EndTimeSteps = SimTimeSteps - 1
            SelectedTimeSteps = np.linspace(StartTimeStep, EndTimeSteps, -NumSelectedTimeSteps, dtype=np.int)
        elif 'FixedVar' in SensitivityType:
            StartTimeStep = SimTimeSteps - 2 + NumSelectedTimeSteps
            EndTimeSteps = SimTimeSteps - 3  # min causality offset = 2; so, for SimTimeSteps=2000, EndTimeSteps=1999-2=1997
            SelectedTimeSteps = np.linspace(StartTimeStep, EndTimeSteps, -NumSelectedTimeSteps, dtype=np.int)
        # SelectedTimeSteps = np.arange(StartTimeStep, SimTimeSteps-2, 1)
    else:  # NumSelectedTimeSteps = 0 for testing
        MidSimTimeSteps = int(SimTimeSteps/2)
        if 'FixedParam' in SensitivityType:
            SelectedTimeSteps = [2,3,MidSimTimeSteps-1,MidSimTimeSteps,MidSimTimeSteps+1,-2,-1]
        elif 'FixedVar' in SensitivityType:
            SelectedTimeSteps = [0,1,MidSimTimeSteps-1,MidSimTimeSteps,MidSimTimeSteps+1,-4,-3]
        NumSelectedTimeSteps = 'test'  # for filename suffix
    if SensitivityType == 'FixedVarTest':
        Variables1 = allStateOuter[-1]
        MidSimTimeSteps = int(SimTimeSteps/2)
        allT = [0,1,MidSimTimeSteps-1,MidSimTimeSteps,MidSimTimeSteps+1,-3]  # regeneration mode
        for t in allT:
            print("t = ", t)
            Parameters1 = allStateInnerOut1[t]
            Parameters2 = allStateInnerOut2[t]
            Parameters3 = allStateOuter[t]

            print("Parameter = InnerOut1")
            Sen = Sensitivity(Variables1, Parameters1)
            SensitivityData = Sen.ComputeGradients()
            # print(SensitivityData[-1,0,1:,:].abs().sum(0))  # for controller 1
            print(SensitivityData[0,0,:,:])  # for controller 1
            for time in allT:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()

            print("Parameter = InnerOut2")
            Sen = Sensitivity(Variables1,Parameters2)
            SensitivityData = Sen.ComputeGradients()
            # print(SensitivityData[-1,0,2:,:].abs().sum(0))  # for controller 2
            print(SensitivityData[3,0,:,:])  # for controller 2
            for time in allT:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()

            print("Parameter = Outer")
            Sen = Sensitivity(Variables1,Parameters3)
            SensitivityData = Sen.ComputeGradients()
            print(SensitivityData)
            for time in allT:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()
            print("-----------------------------------------------------------")
    elif SensitivityType == 'FixedParamSet1':  # this set requires 4 days on the cluster
        # save simulation data files
        fname = './Data/TimeSeriesData_StateOuter_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allStateOuter).detach()
        torch.save(data, fname)
        fname = './Data/TimeSeriesData_WeightOuter_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allWeightOuter).detach()
        torch.save(data, fname)
        fname = './Data/TimeSeriesData_BiasOuter_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allBiasOuter).detach()
        torch.save(data, fname)
        # fname = './Data/TimeSeriesData_StateInnerIn1_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        # data = torch.stack(allStateInnerIn1).detach()
        # torch.save(data, fname)
        # fname = './Data/TimeSeriesData_StateInnerIn2_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        # data = torch.stack(allStateInnerIn2).detach()
        # torch.save(data, fname)
        fname = './Data/TimeSeriesData_StateInnerOut1_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allStateInnerOut1).detach()
        torch.save(data, fname)
        fname = './Data/TimeSeriesData_StateInnerOut2_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allStateInnerOut2).detach()
        torch.save(data, fname)
        fname = './Data/TimeSeriesData_StateOuterTimeConstants_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = torch.stack(allStateOuterTimeConstants).detach()
        torch.save(data, fname)
        # fname = './Data/TimeSeriesData_StateInnerTimeConstants1_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        # data = torch.stack(allStateInnerTimeConstants1).detach()
        # torch.save(data, fname)
        # fname = './Data/TimeSeriesData_StateInnerTimeConstants2_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        # data = torch.stack(allStateInnerTimeConstants2).detach()
        # torch.save(data, fname)
        fname = './Data/TimeSeriesData_NumAliveCells_DynamicSize_MultipleControllers_' + 'Mode' + Mode + '_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
        data = NumAliveCells.detach()
        torch.save(data, fname)
        # do analysis
        Parameters1 = allStateInnerOut1[0]
        Parameters2 = allStateInnerOut2[0]
        Parameters3 = allStateOuter[0]
        FullSensitivityData1_1 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController1,numNodesOuter) * -99
        FullSensitivityData2_2 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController2,numNodesOuter) * -99
        FullSensitivityData1_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        FullSensitivityData2_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        for t in SelectedTimeSteps:
            if Verbose:
                print("Timestep = ", t)

            Variables1 = allStateOuter[t]
            Variables2 = allStateOuterTimeConstants[t]

            Sen = Sensitivity(Variables1,Parameters1)
            savefname = './Data/SensitivityData_StateOuter_StateInnerOut1_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            SensitivityData = Sen.ComputeGradients(save=True,saveFilename=savefname)
            FullSensitivityData1_1[t,:,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            torch.save(FullSensitivityData1_1, fname)
            Parameters1.grad.data.zero_()
            Parameters2.grad.data.zero_()
            Parameters3.grad.data.zero_()

            Sen = Sensitivity(Variables2,Parameters2)
            savefname = './Data/SensitivityData_StateOuterTimeConstants_StateInnerOut2_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            SensitivityData = Sen.ComputeGradients(save=True,saveFilename=savefname)
            FullSensitivityData2_2[t,:,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateInnerOut2_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            torch.save(FullSensitivityData2_2, fname)
            Parameters1.grad.data.zero_()
            Parameters2.grad.data.zero_()
            Parameters3.grad.data.zero_()

            Sen = Sensitivity(Variables1,Parameters3)
            savefname = './Data/SensitivityData_StateOuter_StateOuter_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            SensitivityData = Sen.ComputeGradients(save=True,saveFilename=savefname)
            FullSensitivityData1_3[t,:,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuter_StateOuter_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            torch.save(FullSensitivityData1_3, fname)
            Parameters1.grad.data.zero_()
            Parameters2.grad.data.zero_()
            Parameters3.grad.data.zero_()

            Sen = Sensitivity(Variables2,Parameters3)
            savefname = './Data/SensitivityData_StateOuterTimeConstants_StateOuter_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            SensitivityData = Sen.ComputeGradients(save=True,saveFilename=savefname)
            FullSensitivityData2_3[t,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateOuter_' + 'FixedParam_' + 'Sim' + SimulateType + '_Init' + InitType + '_n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '_Set' + str(SampleSet) + '.dat'
            torch.save(FullSensitivityData2_3, fname)
            Parameters1.grad.data.zero_()
            Parameters2.grad.data.zero_()
            Parameters3.grad.data.zero_()
    elif SensitivityType == 'FixedParamSet2':  # this set requires 4 days on the cluster
        Parameters1 = allStateInnerOut1Sigmoid[1]  # timestep 0 is dummy
        Parameters3 = allStateOuterSigmoid[1]  # timestep 0 is dummy
        FullSensitivityData1_1 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController1,numNodesOuter) * -99
        FullSensitivityData1_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        for t in SelectedTimeSteps:
            if Verbose:
                print(t)

            Variables1 = allStateOuterSigmoid[t]

            Sen = Sensitivity(Variables1,Parameters1)
            SensitivityData = Sen.ComputeGradients()
            FullSensitivityData1_1[t,:,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuterSigmoid_StateInnerOut1Sigmoid_' + 'FixedParam_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            torch.save(FullSensitivityData1_1, fname)
            Parameters1.grad.data.zero_()
            Parameters3.grad.data.zero_()

            Sen = Sensitivity(Variables1,Parameters3)
            SensitivityData = Sen.ComputeGradients()
            FullSensitivityData1_3[t,:,:,:,:] = SensitivityData
            fname = './Data/FullSensitivityData_StateOuterSigmoid_StateOuterSigmoid_' + 'FixedParam_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            torch.save(FullSensitivityData1_3, fname)
            Parameters1.grad.data.zero_()
            Parameters3.grad.data.zero_()
    elif SensitivityType == 'FixedVarSet1':  # this set requires 4 days on the cluster
        Variables1 = allStateOuter[-1]
        Variables2 = allStateOuterTimeConstants[-1]
        FullSensitivityData1_1 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController1,numNodesOuter) * -99
        FullSensitivityData2_2 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController2,numNodesOuter) * -99
        FullSensitivityData1_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        FullSensitivityData2_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        for t in SelectedTimeSteps:
            if Verbose:
                print(t)

            Parameters1 = allStateInnerOut1[t]
            Parameters2 = allStateInnerOut2[t]
            Parameters3 = allStateOuter[t]

            Sen = Sensitivity(Variables1,Parameters1)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            if os.path.exists(fname):
                FullSensitivityData1_1 = torch.load(fname)
            FullSensitivityData1_1[t,:,:,:,:] = SensitivityData
            torch.save(FullSensitivityData1_1, fname)
            for time in SelectedTimeSteps:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()

            Sen = Sensitivity(Variables2,Parameters2)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateInnerOut2_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            if os.path.exists(fname):
                FullSensitivityData2_2 = torch.load(fname)
            FullSensitivityData2_2[t,:,:,:,:] = SensitivityData
            torch.save(FullSensitivityData2_2, fname)
            for time in SelectedTimeSteps:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()

            Sen = Sensitivity(Variables1,Parameters3)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuter_StateOuter_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            if os.path.exists(fname):
                FullSensitivityData1_3 = torch.load(fname)
            FullSensitivityData1_3[t,:,:,:,:] = SensitivityData
            torch.save(FullSensitivityData1_3, fname)
            for time in SelectedTimeSteps:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()

            Sen = Sensitivity(Variables2,Parameters3)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateOuter_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            if os.path.exists(fname):
                FullSensitivityData2_3 = torch.load(fname)
            FullSensitivityData2_3[t,:,:,:] = SensitivityData
            torch.save(FullSensitivityData2_3, fname)
            for time in SelectedTimeSteps:
                allStateInnerOut1[time].grad.data.zero_()
                allStateInnerOut2[time].grad.data.zero_()
                allStateOuter[time].grad.data.zero_()
    elif SensitivityType == 'FixedVarSet2':  # this set requires 4 days on the cluster
        Variables1 = allStateOuterSigmoid[-1]
        FullSensitivityData1_1 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numOutNodesController1,numNodesOuter) * -99
        FullSensitivityData1_3 = torch.ones(SimTimeSteps,numNodesOuter,numInitStates,numNodesOuter,1) * -99
        for t in SelectedTimeSteps[2:]:  # timesteps 0,1 are dummy
            if Verbose:
                print(t)

            Parameters1 = allStateInnerOut1Sigmoid[t]
            Parameters3 = allStateOuterSigmoid[t]

            Sen = Sensitivity(Variables1,Parameters1)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuterSigmoid_StateInnerOut1Sigmoid_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            FullSensitivityData1_1[t,:,:,:,:] = SensitivityData
            torch.save(FullSensitivityData1_1, fname)
            for time in SelectedTimeSteps[2:]:  # timesteps 0,1 are dummy
                allStateInnerOut1Sigmoid[time].grad.data.zero_()
                allStateOuterSigmoid[time].grad.data.zero_()

            Sen = Sensitivity(Variables1,Parameters3)
            SensitivityData = Sen.ComputeGradients()
            fname = './Data/FullSensitivityData_StateOuterSigmoid_StateOuterSigmoid_' + 'FixedVar_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
            FullSensitivityData1_3[t,:,:,:,:] = SensitivityData
            torch.save(FullSensitivityData1_3, fname)
            for time in SelectedTimeSteps[2:]:  # timesteps 0,1 are dummy
                allStateInnerOut1Sigmoid[time].grad.data.zero_()
                allStateOuterSigmoid[time].grad.data.zero_()
elif Mode == 'Simulate':
    allStateOuter, allWeightOuter, allBiasOuter, allStateInnerOut1, allStateInnerOut2, \
        allStateInnerIn1, allStateInnerIn2, StateOuter, StateInnerIn1, StateInnerIn2, \
        StateInnerOut1, StateInnerOut2, B_outer, W_outer, \
        allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2,\
        allStateOuterSigmoid, allStateInnerOut1Sigmoid, NumAliveCells = MasterAnalyze()
    fname = './Data/TimeSeriesData_StateOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateOuter, fname)
    fname = './Data/TimeSeriesData_WeightOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allWeightOuter, fname)
    fname = './Data/TimeSeriesData_BiasOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allBiasOuter, fname)
    fname = './Data/TimeSeriesData_StateInnerIn1_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerIn1, fname)
    fname = './Data/TimeSeriesData_StateInnerIn2_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerIn2, fname)
    fname = './Data/TimeSeriesData_StateInnerOut1_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerOut1, fname)
    fname = './Data/TimeSeriesData_StateInnerOut2_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerOut2, fname)
    fname = './Data/TimeSeriesData_StateOuterTimeConstants_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateOuterTimeConstants, fname)
    fname = './Data/TimeSeriesData_StateInnerTimeConstants1_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerTimeConstants1, fname)
    fname = './Data/TimeSeriesData_StateInnerTimeConstants2_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(allStateInnerTimeConstants2, fname)
    fname = './Data/TimeSeriesData_NumAliveCells_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
    torch.save(NumAliveCells, fname)
elif Mode == 'Animate':
    if AnimateType == 'StateOuterTimeSeries':
        numStopCells = 2
        numNodesOuter = numCells + numStopCells
        fname = './Data/TimeSeriesData_StateOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(FileNumber) + '.dat'
        allStateOuter1List = torch.load(fname)
        NumTimeSteps = len(allStateOuter1List)
        # numNodesOuter = allStateOuter1List[0].shape[1]
        TargetPattern = torch.FloatTensor(np.linspace(1,-1,numNodesOuter-numStopCells))
        TargetPattern = torch.cat((torch.FloatTensor([0]),TargetPattern,torch.FloatTensor([0]))).view(1,numNodesOuter)
        allStateOuter1 = torch.zeros(*allStateOuter1List[0][:,:,0].shape,NumTimeSteps)  # shape = (NumSamples,NumNodesOuter,NumTimeSteps)
        for t in range(NumTimeSteps):
            allStateOuter1[:,:,t] = allStateOuter1List[t][:,:,0]
        allStateOuter1[0,:,:] = allStateOuter1[0,:,:]/(allStateOuter1[0,:,:].abs().max(0).values)
        allStateOuter1 = allStateOuter1[0,:,:]  # shape = (NumNodesOuter,NumTimeSteps)
        if NumTimeSteps == ((SimTimeSteps*2)+2):
            allStateOuter1 = torch.cat((allStateOuter1[:,0].repeat(299,1).t().view(numNodesOuter,-1),
                                        allStateOuter1[:,1:2001],
                                        allStateOuter1[:,2001].repeat(299,1).t().view(numNodesOuter,-1),
                                        allStateOuter1[:,2002:]),dim=1)  # repeat initial state 300 times so as to match the network animation
        else:
            allStateOuter1 = torch.cat((allStateOuter1[:,0].repeat(299,1).t().view(numNodesOuter,-1),allStateOuter1),dim=1)  # repeat initial state 300 times so as to match the network animation
        savefname = './Data/StateOuterTimeSeriesMovie_DynamicSize_MultipleControllers_' + str(FileNumber) + '.mp4'
        animateTimeSeries(allStateOuter1[:,0:],TargetPattern[0],savefname)
    elif AnimateType == 'Sensitivity':
        if NumSelectedTimeSteps > 0:
            if 'FixedParam' in SensitivityType:
                StartTimeStep = 2
                EndTimeStep = SimTimeSteps - 1
                SelectedTimeSteps = np.linspace(StartTimeStep, EndTimeStep, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedParam_'
            elif 'FixedVar' in SensitivityType:
                StartTimeStep = 0
                EndTimeStep = SimTimeSteps - 3  # min causality offset = 2; so, for SimTimeSteps=2000, EndTimeSteps=1999-2=1997
                SelectedTimeSteps = np.linspace(StartTimeStep, EndTimeStep, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedVar_'
        # fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuter_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateInnerOut2_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuterSigmoid_StateInnerOut1Sigmoid_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuterSigmoid_StateOuterSigmoid_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        FullSensitivityData = torch.load(fname)  # shape = (NumTimeSteps,NumCells,NumSamples,NumCells,1) # shape = (NumTimeSteps,NumCells,NumSamples,NumCells,1)
        numNodesOuter = FullSensitivityData.shape[1]
        offset = 0
        # numPlotTimeSteps = len(SelectedTimeSteps[offset:])
        numPlotTimeSteps = EndTimeStep - StartTimeStep + 1
        heatmap = torch.zeros(numPlotTimeSteps, numNodesOuter, numNodesOuter)
        # for t in SelectedTimeSteps[offset:]:
        for t in range(NumSelectedTimeSteps):
            for causingcell in range(numNodesOuter):
                for causedcell in range(numNodesOuter):
                    # heatmap[SelectedTimeSteps[t]-StartTimeStep, causingcell, causedcell] = FullSensitivityData[SelectedTimeSteps[t], causedcell, 0, 1:, causingcell].abs().sum()
                    heatmap[SelectedTimeSteps[t]-StartTimeStep, causingcell, causedcell] = FullSensitivityData[SelectedTimeSteps[t], causedcell, 0, causingcell, 0].abs()
            timewiseMax = heatmap[SelectedTimeSteps[t]-StartTimeStep].max()
            timewiseMin = heatmap[SelectedTimeSteps[t]-StartTimeStep].min()
            heatmap[SelectedTimeSteps[t]-StartTimeStep] = (heatmap[SelectedTimeSteps[t]-StartTimeStep] - timewiseMin) / (timewiseMax - timewiseMin)
        # savefname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        # savefname = './Data/FullSensitivityData_StateOuter_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        # savefname = './Data/FullSensitivityData_StateOuterTimeConstants_StateInnerOut2_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        savefname = './Data/FullSensitivityData_StateOuterTimeConstants_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        # savefname = './Data/FullSensitivityData_StateOuterSigmoid_StateInnerOut1Sigmoid_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        # savefname = './Data/FullSensitivityData_StateOuterSigmoid_StateOuterSigmoid_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        if 'FixedParam' in SensitivityType:
            animateHeatmap(heatmap,savefname)
        elif 'FixedVar' in SensitivityType:
            # heatmap = torch.flip(heatmap,[0])  # reverse the timeseries
            animateHeatmap(heatmap, savefname)
    elif AnimateType == 'SensitivityDifference':
        if NumSelectedTimeSteps > 0:
            if 'FixedParam' in SensitivityType:
                EndTimeSteps = SimTimeSteps - 1
                SelectedTimeSteps = np.linspace(2, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedParam_'
            elif 'FixedVar' in SensitivityType:
                EndTimeSteps = SimTimeSteps - 3  # min causality offset = 2; so, for SimTimeSteps=2000, EndTimeSteps=1999-2=1997
                SelectedTimeSteps = np.linspace(0, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedVar_'
        Sigmoid = False
        if Sigmoid:
            SigmoidType = 'Sigmoid'
        else:
            SigmoidType = ''
        fname1 = './Data/FullSensitivityData_StateOuter' + SigmoidType + '_StateInnerOut1' + SigmoidType + '_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        fname2 = './Data/FullSensitivityData_StateOuter' + SigmoidType + '_StateOuter' + SigmoidType + '_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        FullSensitivityData1 = torch.load(fname1)  # shape = (NumTimeSteps,NumCells,NumSamples,numOutNodesController1,NumCells)
        FullSensitivityData2 = torch.load(fname2)  # shape = (NumTimeSteps,NumCells,NumSamples,NumCells,1)
        numNodesOuter = FullSensitivityData1.shape[1]
        numPlotTimeSteps = len(SelectedTimeSteps)
        heatmap = torch.zeros(numPlotTimeSteps, numNodesOuter, numNodesOuter)
        for t in range(numPlotTimeSteps):
            for causingcell in range(numNodesOuter):
                for causedcell in range(numNodesOuter):
                    SensitivityDifference = FullSensitivityData2[SelectedTimeSteps[t], causedcell, 0, causingcell, 0].abs() - \
                                            FullSensitivityData1[SelectedTimeSteps[t], causedcell, 0, 1:, causingcell].abs().sum()
                    heatmap[t, causingcell, causedcell] = SensitivityDifference
            timewiseMax = heatmap[t].max()
            timewiseMin = heatmap[t].min()
            heatmap[t] = (heatmap[t] - timewiseMin) / (timewiseMax - timewiseMin)
            # scale = torch.ones(heatmap[t].shape)
            # scale[heatmap[t] < 0] = timewiseMin
            # scale[heatmap[t] > 0] = timewiseMax
            # heatmap[t] = heatmap[t] / scale
        heatmap[heatmap!=heatmap] = 0.5
        savefname = './Data/FullSensitivityData_StateOuter' + SigmoidType + '__StateOuter' + SigmoidType + '_StateInnerOut1' + SigmoidType + '_Difference_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.mp4'
        animateHeatmap(heatmap[2:],savefname)
elif Mode == 'Plot':
    if PlotType == 'Sensitivity':
        if NumSelectedTimeSteps > 0:
            if 'FixedParam' in SensitivityType:
                EndTimeSteps = SimTimeSteps - 1
                SelectedTimeSteps = np.linspace(2, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedParam_'
            elif 'FixedVar' in SensitivityType:
                EndTimeSteps = SimTimeSteps - 3  # min causality offset = 2; so, for SimTimeSteps=2000, EndTimeSteps=1999-2=1997
                SelectedTimeSteps = np.linspace(0, EndTimeSteps, NumSelectedTimeSteps, dtype=np.int)
                Type = 'FixedVar_'
        # fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_FixedParam_SimRescale_InitDefault_n22_1999_74.dat'
        # fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_FixedParam_SimDefault_InitHomogeneousUppertau_n12_2499_74.dat'
        # fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_FixedParam_SimDefault_InitHomogeneousLowertau_n12_2499_74.dat'
        fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_FixedParam_SimRegenerate_InitDefault_n12_1999_74.dat'
        # fname = './Data/FullSensitivityData_StateOuter_StateInnerOut1_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuter_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateInnerOut2_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # fname = './Data/FullSensitivityData_StateOuterTimeConstants_StateOuter_' + Type + str(NumSelectedTimeSteps) + '_' + str(FileNumber) + '.dat'
        # FullSensitivityData = torch.load(fname)  # shape = (NumTimeSteps,NumCells,NumSamples,numOutNodesController1,NumCells)
        FullSensitivityData = torch.load(fname)  # shape = (NumTimeSteps,NumCells,NumSamples,NumCells,1)
        numNodesOuter = FullSensitivityData.shape[1]

        numPlotTimeSteps = len(SelectedTimeSteps)
        numOutNodesController1 = FullSensitivityData.shape[3]  # works only for FullSensitivityData_*_StateInnerOut1_

        # heatmapRepresenter = torch.zeros(numPlotTimeSteps, numNodesOuter)
        # heatmapRepresented = torch.zeros(numPlotTimeSteps, numNodesOuter)
        # for node in range(numNodesOuter):
        #     for t in range(numPlotTimeSteps):
        #         heatmapRepresenter[t, node] = FullSensitivityData[SelectedTimeSteps[t], :, 0, 1:, node].abs().sum()  # full sum
        #         heatmapRepresented[t, node] = FullSensitivityData[SelectedTimeSteps[t], node, 0, 1:, :].abs().sum()  # full sum
        #         # heatmapRepresenter[t, node] = FullSensitivityData[SelectedTimeSteps[t], node, 0, :, 0].abs().sum()
        # timewiseMaxRepresenter = heatmapRepresenter.max(1).values.view(-1, 1)
        # timewiseMinRepresenter = heatmapRepresenter.min(1).values.view(-1, 1)
        # heatmapRepresenter = (heatmapRepresenter - timewiseMinRepresenter) / (timewiseMaxRepresenter - timewiseMinRepresenter)
        # timewiseMaxRepresented = heatmapRepresented.max(1).values.view(-1, 1)
        # timewiseMinRepresented = heatmapRepresented.min(1).values.view(-1, 1)
        # heatmapRepresented = (heatmapRepresented - timewiseMinRepresented) / (timewiseMaxRepresented - timewiseMinRepresented)
        # heatmap = heatmapRepresented - heatmapRepresenter

        # heatmapRepresenter = torch.zeros(numPlotTimeSteps, numNodesOuter)
        # # heatmapRepresented = torch.zeros(numPlotTimeSteps, numNodesOuter)
        # for node in range(numNodesOuter):
        #     for t in range(numPlotTimeSteps):
        #         heatmapRepresenter[t, node] = FullSensitivityData[SelectedTimeSteps[t], node, 0, 8, 11].sum()  # full sum
        #         # heatmapRepresenter[t, node] = FullSensitivityData[SelectedTimeSteps[t], node, 0, 6, 0].abs().sum()
        # timewiseMaxRepresenter = heatmapRepresenter.max(1).values.view(-1, 1)
        # timewiseMinRepresenter = heatmapRepresenter.min(1).values.view(-1, 1)
        # scaling = (timewiseMaxRepresenter - timewiseMinRepresenter)
        # heatmapRepresenter = (heatmapRepresenter - timewiseMinRepresenter) / scaling
        # heatmapRepresenter[heatmapRepresenter!=heatmapRepresenter] = 0.5
        # heatmap = heatmapRepresenter

        # heatmapFatemap = torch.zeros(numNodesOuter, numNodesOuter)
        # for node in range(numNodesOuter):
        #     heatmapFatemap[node, :] = FullSensitivityData[1200, node, 0, 1:, :].abs().sum(0)
        # cellwiseMax = heatmapFatemap.max(1).values.view(-1, 1)
        # cellwiseMin = heatmapFatemap.min(1).values.view(-1, 1)
        # heatmap = (heatmapFatemap - cellwiseMin) / (cellwiseMax - cellwiseMin)

        # from scipy.ndimage.filters import gaussian_filter
        # # heatmap_smooth = gaussian_filter(heatmap,sigma=1)
        # fig, _ = plt.subplots()
        # ax = sns.heatmap(heatmap, cmap="coolwarm")
        # # ax = sns.kdeplot(heatmap, cmap="coolwarm")
        # plt.xlabel('Causing cell', fontsize=18)
        # plt.ylabel('Time', fontsize=18)
        # fig.tight_layout()
        # plt.show()

        # Generate and save a variety of heatmaps (both heatmap arrays and images)
        heatmapRepresenterNode = torch.zeros(numPlotTimeSteps, numNodesOuter, numNodesOuter)
        heatmapRepresenterNodeGene = torch.zeros(numPlotTimeSteps, numNodesOuter, numOutNodesController1-1, numNodesOuter)  # -1 removes the outer state which is not part of the controller
        for representer in range(numNodesOuter):
            for represented in range(numNodesOuter):
                for t in range(numPlotTimeSteps):
                    heatmapRepresenterNode[t, represented, representer] = FullSensitivityData[SelectedTimeSteps[t], represented, 0, 1:, representer].sum()  # full sum
                    heatmapRepresenterNodeGene[t, represented, :, representer] = FullSensitivityData[SelectedTimeSteps[t], represented, 0, 1:, representer]
            timewiseMaxRepresenterNode = heatmapRepresenterNode[:,:,representer].max(-1).values.view(-1, 1)
            timewiseMinRepresenterNode = heatmapRepresenterNode[:,:,representer].min(-1).values.view(-1, 1)
            heatmapRepresenterNode[:,:,representer] = (heatmapRepresenterNode[:,:,representer] - timewiseMinRepresenterNode) / (timewiseMaxRepresenterNode - timewiseMinRepresenterNode)
            for gene in range(0,numOutNodesController1-1):
                timewiseMaxRepresenterNodeGene = heatmapRepresenterNodeGene[:,:,gene,representer].max(-1).values.view(-1, 1)
                timewiseMinRepresenterNodeGene = heatmapRepresenterNodeGene[:,:,gene,representer].min(-1).values.view(-1, 1)
                heatmapRepresenterNodeGene[:,:,gene,representer] = (heatmapRepresenterNodeGene[:,:,gene,representer] - timewiseMinRepresenterNodeGene) / (timewiseMaxRepresenterNodeGene - timewiseMinRepresenterNodeGene)
        heatmapRepresenterNode[heatmapRepresenterNode != heatmapRepresenterNode] = 0.5
        heatmapRepresenterNodeGene[heatmapRepresenterNodeGene != heatmapRepresenterNodeGene] = 0.5
        torch.save(heatmapRepresenterNode,'./Data/heatmapRepresenterNode_Regenerate.dat')
        torch.save(heatmapRepresenterNodeGene,'./Data/heatmapRepresenterNodeGene_Regenerate.dat')
        # fig, axs = plt.subplots(numOutNodesController1-1, numNodesOuter)
        # for gene in range(1,numOutNodesController1):
        #     for representer in range(numNodesOuter):
        #         print(representer,gene)
        #         title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
        #         heatmap = heatmapRepresenterNodeGene[:,:,gene-1,representer]
        #         sns.heatmap(heatmap, cmap="coolwarm",ax=axs[gene-1,representer],cbar=False,xticklabels=False,yticklabels=False)
        #         # axs[gene-1,representer].set_title(title)
        # savefname = 'IndividualGeneInfluence_all.png'
        # plt.savefig('./Data/' + savefname)
        fig, axs = plt.subplots(1,numNodesOuter)
        for representer in range(numNodesOuter):
            print(representer)
            title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
            heatmap = heatmapRepresenterNode[:,:,representer]
            sns.heatmap(heatmap, cmap="coolwarm",ax=axs[representer],cbar=False,xticklabels=False,yticklabels=False,square=False)
            # axs[representer].set_title(title)
        savefname = 'TotalGeneInfluence_all_FixedParam_Regenerate.png'
        plt.savefig('./Data/' + savefname)
        # for representer in range(numNodesOuter):
        #     print(representer)
        #     xlabel = 'Caused cell'
        #     ylabel = 'Time'
        #     title = 'Causing cell = ' + str(representer)
        #     savefname = 'TotalGeneInfluence_' + 'Cell' + str(representer) + '.png'
        #     heatmap = heatmapRepresenterNode[:,:,representer]
        #     makeHeatmap(heatmap,xlabel,ylabel,title,savefname)
        # for representer in range(numNodesOuter):
        #     for gene in range(1,numOutNodesController1):
        #         print(representer,gene)
        #         xlabel = 'Caused cell'
        #         ylabel = 'Time'
        #         title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
        #         savefname = 'IndividualGeneInfluence_' + 'Cell' + str(representer) + '_Gene' + str(gene) + '.png'
        #         heatmap = heatmapRepresenterNodeGene[:,:,gene-1,representer]
        #         makeHeatmap(heatmap,xlabel,ylabel,title,savefname)
    elif PlotType == 'Weights':  # run Mode Simulate at least once to generate the requisite data files
        numNodesOuter = numCells + 2
        fname = './Data/TimeSeriesData_WeightOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(FileNumber) + '.dat'
        weights = torch.load(fname)  # shape = (SimTimeSteps, numEdges, 1)
        weights = torch.stack(weights)
        weights = weights[:,:,0]
        numInterWeights = numNodesOuter - 1
        numSelfWeights = numNodesOuter
        interWeights = weights[:,0:numInterWeights]
        selfWeights = weights[:,numInterWeights:]
        timewiseMaxInter = interWeights.max(-1).values.view(-1, 1)
        timewiseMinInter = interWeights.min(-1).values.view(-1, 1)
        interWeightsHeatmap = (interWeights - timewiseMinInter) / (timewiseMaxInter - timewiseMinInter)
        timewiseMaxSelf = selfWeights.max(-1).values.view(-1, 1)
        timewiseMinSelf = selfWeights.min(-1).values.view(-1, 1)
        selfWeightsHeatmap = (selfWeights - timewiseMinSelf) / (timewiseMaxSelf - timewiseMinSelf)
        fig, axs = plt.subplots(1,1)
        # title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
        sns.heatmap(interWeightsHeatmap, cmap="coolwarm",ax=axs,cbar=False,xticklabels=False,yticklabels=False,square=False)
        # sns.heatmap(selfWeightsHeatmap, cmap="coolwarm",ax=axs[1],cbar=False,xticklabels=False,yticklabels=False,square=False)
        plt.show()
    elif PlotType == 'Bias':  # run Mode Simulate at least once to generate the requisite data files
        numNodesOuter = numCells + 2
        fname = './Data/TimeSeriesData_BiasOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(FileNumber) + '.dat'
        bias = torch.load(fname)  # shape = (SimTimeSteps, 1, numNodesOuter, 1)
        bias = torch.stack(bias)
        bias = bias[:,0,:,0]
        timewiseMax = bias.max(-1).values.view(-1, 1)
        timewiseMin = bias.min(-1).values.view(-1, 1)
        biasHeatmap = (bias - timewiseMin) / (timewiseMax - timewiseMin)
        fig, axs = plt.subplots(1,1)
        # title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
        sns.heatmap(biasHeatmap, cmap="coolwarm",ax=axs,cbar=False,xticklabels=False,yticklabels=False,square=False)
        plt.show()
    elif PlotType == 'OuterState':  # run Mode Simulate at least once to generate the requisite data files
        numNodesOuter = numCells + 2
        fname = './Data/TimeSeriesData_StateOuter_DynamicSize_MultipleControllers_' + 'n' + str(numNodesOuter) + '_' + str(FileNumber) + '.dat'
        outerstate = torch.load(fname)  # shape = (SimTimeSteps, 1, numNodesOuter,1)
        outerstate = torch.stack(outerstate)
        outerstate = outerstate[:,0,:,0]
        timewiseMax = outerstate.max(-1).values.view(-1, 1)
        timewiseMin = outerstate.min(-1).values.view(-1, 1)
        outerstateHeatmap = (outerstate - timewiseMin) / (timewiseMax - timewiseMin)
        fig, axs = plt.subplots(1,1)
        # title = 'Causing cell = ' + str(representer) + ', causing gene = ' + str(gene)
        sns.heatmap(outerstateHeatmap, cmap="coolwarm",ax=axs,cbar=False,xticklabels=False,yticklabels=False,square=False)
        plt.show()
