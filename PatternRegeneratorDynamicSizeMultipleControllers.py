# A regenerative network with local controllers.
# The system consists of an 'outer' network that regenerates and preserves patterns, with each cell in this outer network
# consisting of an 'inner' network that helps regenerate the weights of the outer cell that it resides in.
# The inner network is referred to as the 'local' controller; it's the same network (same connectivity, weights and bias)
# that resides in every outer cell.

# This version: independent intracellular controller for controlling life constants

import torch
import numpy as np
import math
import argparse

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--Mode', type=str, default='Learn')  # Options: Simulate, Learn, LearnPhase1Only
parser.add_argument('--Stochastic', action='store_true')
parser.add_argument('--ResumeLearning', action='store_true')
parser.add_argument('--numCells', type=int, default=10)
parser.add_argument('--numInitStates', type=int, default=1)
parser.add_argument('--targetUpperTimeConstant', type=int, default=2)
parser.add_argument('--LearnIters', type=int, default=100)
parser.add_argument('--SimTimeSteps', type=int, default=2000)
parser.add_argument('--EvaluationProp', type=float, default=0.15)
parser.add_argument('--FileNumber', type=int, default=0)
parser.add_argument('--Verbose', action='store_true')
args = parser.parse_args()
Mode = args.Mode
Stochastic = args.Stochastic
ResumeLearning = args.ResumeLearning
numCells = args.numCells
numInitStates = args.numInitStates
targetUpperTimeConstant = args.targetUpperTimeConstant
LearnIters = args.LearnIters
SimTimeSteps = args.SimTimeSteps
EvaluationProp = args.EvaluationProp
FileNumber = args.FileNumber
Verbose = args.Verbose

def computeLatticeAdjacencyMatrix(LatticeDims):
    numrows = LatticeDims[0]
    numcols = LatticeDims[1]
    rowIndices = np.tile(np.arange(0, numrows), numcols)
    colIndices = np.repeat(np.arange(0, numcols), numrows)
    numCellsLayer = numrows * numcols
    cellIndices = np.arange(numCellsLayer)
    AdjMatrix = np.zeros((numCellsLayer, numCellsLayer), dtype=np.int8)
    for cell in cellIndices:
        r = rowIndices[cell]
        c = colIndices[cell]
        rowNeighbors = cellIndices[(rowIndices == r) & (np.abs(colIndices - c) == 1)]
        colNeighbors = cellIndices[(colIndices == c) & (np.abs(rowIndices - r) == 1)]
        AdjMatrix[cell, rowNeighbors] = 1
        AdjMatrix[cell, colNeighbors] = 1
    return (AdjMatrix)

# controller1 - GRN
def computeInterLayerAdjacenyMatrixGenetic(layer1, layer2, LayeringController, CellIndicesPerLayer, numLayer1NodesControllerIn,
                                           numLayer1NodesControllerOut, numLayer1NodesControllerBidir, numLayer2NodesController):
    NumLayer2CellsToConnect = LayeringController[layer2][0]  # a cell in layer 1 connects only to a single column of layer 2
    # Note: the way by which we compute Layer2ColsToConnect for the in and out cells is not efficient;
    # it may not work as expected if we include more in and out cells in various proportions
    # Construct inter-adj matrix for 'in' and 'bidir' cells
    AdjMatrixIn = np.zeros((numLayer1NodesControllerIn + numLayer1NodesControllerBidir, numLayer2NodesController))
    offset = CellIndicesPerLayer[layer2][0]
    Layer2ColsToConnect = np.array([0,0,1,2])  # (hard-coding) # The corresponding variables are: left weight, bias (later connects from all cells), self weight and right weight
    for cell in range(numLayer1NodesControllerIn + numLayer1NodesControllerBidir):
        col = Layer2ColsToConnect[cell]
        st = col*NumLayer2CellsToConnect
        nd = st + NumLayer2CellsToConnect
        FromCells = np.array(CellIndicesPerLayer[layer2][st:nd]) - offset
        AdjMatrixIn[cell, FromCells] = 1
    BiasCell = 1  # order of 'in' cells: left weight, bias, self weight and right weight # (hard-coding)
    FromCellsForBias = np.array(CellIndicesPerLayer[layer2]) - offset
    AdjMatrixIn[BiasCell,FromCellsForBias] = 1  # outer bias is determined by all inner controller layer 2 cells
    # Construct inter-adj matrix for 'out' and 'bidir' cells
    AdjMatrixOut = np.zeros((numLayer2NodesController, numLayer1NodesControllerOut + numLayer1NodesControllerBidir))
    # NOTE: outer state is the only 'out' node at the moment
    StateCell = 0 # (hard-coding)
    ToCellsForState = np.array(CellIndicesPerLayer[layer2]) - offset  # outer state influences all cells in the controller
    AdjMatrixOut[ToCellsForState, StateCell] = 1  # state cell is connected to all cells in the inner controller layer 2
    return(AdjMatrixIn,AdjMatrixOut)

# controller2 - life constants (tau)
def computeInterLayerAdjacenyMatrixLife(layer1, layer2, LayeringController, CellIndicesPerLayer, numLayer1NodesControllerIn,
                                        numLayer1NodesControllerOut, numLayer1NodesControllerBidir, numLayer2NodesController):
    NumLayer2CellsToConnect = LayeringController[layer2][0]  # a cell in layer 1 connects only to a single column of layer 2
    # Note: the way by which we compute Layer2ColsToConnect for the in and out cells is not efficient;
    # it may not work as expected if we include more in and out cells in various proportions
    # Construct inter-adj matrix for 'in' and 'bidir' cells
    AdjMatrixIn = np.zeros((numLayer1NodesControllerIn + numLayer1NodesControllerBidir, numLayer2NodesController))
    offset = CellIndicesPerLayer[layer2][0]
    Layer2ColsToConnect = np.array([0,1,2])  # (hard-coding) # The corresponding variables are: left tau, self tau and right tau
    for cell in range(numLayer1NodesControllerIn + numLayer1NodesControllerBidir):
        col = Layer2ColsToConnect[cell]
        st = col*NumLayer2CellsToConnect
        nd = st + NumLayer2CellsToConnect
        FromCells = np.array(CellIndicesPerLayer[layer2][st:nd]) - offset
        AdjMatrixIn[cell, FromCells] = 1
    # NOTE: unlike controller1, the bias cell is connected only to the second column of layer2
    # Construct inter-adj matrix for 'out' and 'bidir' cells
    AdjMatrixOut = np.zeros((numLayer2NodesController, numLayer1NodesControllerOut + numLayer1NodesControllerBidir))
    # The 'out' variables are: outer state and self tau  # (hard-coding)
    ToCellsForState = np.array(CellIndicesPerLayer[layer2]) - offset  # outer state and self-tau influence all cells in the controller
    for cell in range(numLayer1NodesControllerOut + numLayer1NodesControllerBidir):
        AdjMatrixOut[ToCellsForState, cell] = 1  # connected to all cells in the inner controller layer 2
    return(AdjMatrixIn,AdjMatrixOut)

def computeControllerAdjacencyMatrix(LayeringController, numLayer1NodesControllerIn, numLayer1NodesControllerBidir,
                                     numLayer2NodesController, numLayer1NodesControllerOut,
                                     CellIndicesPerLayer, ControllerType):  # the 'inner' contains the controller
    FullAdjMatrix = np.zeros((numLayer1NodesControllerIn+numLayer1NodesControllerBidir+numLayer2NodesController,
                              numLayer1NodesControllerOut+numLayer1NodesControllerBidir+numLayer2NodesController), dtype=np.uint8)
    # Compute connectivity of individual inner layers
    # layer 0: no connectivity within itself
    # layer 1: lattice
    layer = 1
    LatticeAdjMatrix = computeLatticeAdjacencyMatrix(LayeringController[layer])  # currently we assume that only the controller is a lattice
    start1 = numLayer1NodesControllerIn + numLayer1NodesControllerBidir
    end1 = start1 + numLayer2NodesController
    start2 = numLayer1NodesControllerOut + numLayer1NodesControllerBidir
    end2 = start2 + numLayer2NodesController
    FullAdjMatrix[start1:end1, start2:end2] = LatticeAdjMatrix
    # Compute connectivity between adjacent pairs of layers contained within the inner controller layer
    # (2) the inner controller layer consisting of two layers:
    # a) a lattice of cells and
    # b) the weights, the bias and the state of a single outer layer cell connected to that lattice
    LayerPairs = [(0, 1)]
    for layer1, layer2 in LayerPairs:
        if ControllerType == 'GRN':
            InterLayerAdjMatrixIn, InterLayerAdjMatrixOut = \
                computeInterLayerAdjacenyMatrixGenetic(layer1, layer2, LayeringController, CellIndicesPerLayer, numLayer1NodesControllerIn,
                                                       numLayer1NodesControllerOut, numLayer1NodesControllerBidir,numLayer2NodesController)
        elif ControllerType == 'Life':
            InterLayerAdjMatrixIn, InterLayerAdjMatrixOut = \
                computeInterLayerAdjacenyMatrixLife(layer1, layer2, LayeringController, CellIndicesPerLayer, numLayer1NodesControllerIn,
                                                       numLayer1NodesControllerOut, numLayer1NodesControllerBidir,numLayer2NodesController)
        start1, end1 = 0, numLayer1NodesControllerIn + numLayer1NodesControllerBidir
        start2 = numLayer1NodesControllerOut + numLayer1NodesControllerBidir
        end2 = start2 + numLayer2NodesController
        FullAdjMatrix[start1:end1, start2:end2] = InterLayerAdjMatrixIn
        start1 = numLayer1NodesControllerIn + numLayer1NodesControllerBidir
        end1 = start1 + numLayer2NodesController
        start2, end2 = 0, numLayer1NodesControllerOut + numLayer1NodesControllerBidir
        FullAdjMatrix[start1:end1, start2:end2] = InterLayerAdjMatrixOut
    return (FullAdjMatrix)

def ComputeLearningRate(params,paramType='weight'):
    if paramType=='weight':
        max_delta_exp = -2
    elif paramType=='bias':
        max_delta_exp = -2
    elif paramType=='timeconstant':
        max_delta_exp = -2
    elif paramType=='range':
        max_delta_exp = -1
    params.grad.data[params.grad.data!=params.grad.data] = 0  # replace NaNs with 0
    ParamsNan = (params.grad.data == 0)
    if (ParamsNan.any()):
        numNan = ParamsNan.sum().item()
        params_grad_noise = torch.normal(mean=0.0, std=torch.ones(numNan)*1e-5).view(1,-1)  # mainly to shake it from saturation
        params.grad.data[ParamsNan] = params_grad_noise[0]
    params_learning_rate = 10**((torch.max(np.floor(np.log10(np.abs(params.grad.data))))*-1)+max_delta_exp)  # max delta for weight
    return(params_learning_rate)

# Define Weight and bias tensors from the above for simulation
def InstantiateSimulationParametersConstants(numSamples):
    # Parameters
    # Controller 1
    AdjInnerCore1 = AdjInner1.clone()
    start1 = numLayer1NodesController1In
    end1 = start1 + numLayer1NodesController1Bidir + numLayer2NodesController1
    start2 = numLayer1NodesController1Out
    end2 = start2 + numLayer1NodesController1Bidir + numLayer2NodesController1
    AdjInnerCore1[start1:end1, start2:end2] = torch.triu(AdjInnerCore1[start1:end1, start2:end2])
    W_inner1 = AdjInnerCore1.clone()
    x,y = np.where(W_inner1==1)
    W_inner1[x,y] = ParWeightsInner1.clone()
    W_inner1[start1:end1, start2:end2] = W_inner1[start1:end1, start2:end2] + W_inner1[start1:end1, start2:end2].t()
    B_inner1 = ParBiasInner1.clone().view(-1,1)
    # Controller 2
    AdjInnerCore2 = AdjInner2.clone()
    start1 = numLayer1NodesController2In
    end1 = start1 + numLayer1NodesController2Bidir + numLayer2NodesController2
    start2 = numLayer1NodesController2Out
    end2 = start2 + numLayer1NodesController2Bidir + numLayer2NodesController2
    AdjInnerCore2[start1:end1, start2:end2] = torch.triu(AdjInnerCore2[start1:end1, start2:end2])
    W_inner2 = AdjInnerCore2.clone()
    x, y = np.where(W_inner2 == 1)
    W_inner2[x, y] = ParWeightsInner2.clone()
    W_inner2[start1:end1, start2:end2] = W_inner2[start1:end1, start2:end2] + W_inner2[start1:end1, start2:end2].t()
    B_inner2 = ParBiasInner2.clone().view(-1, 1)
    if CellType == 'HeterogeneousCellType':
        B_outer = ParBiasOuter.clone().view(-1,1)
    elif CellType == 'HomogeneousCellType':
        B_outer = ParBiasOuter.clone().repeat(numNodesOuter, 1)
    elif CellType == 'DynamicCellType':
        B_outer = (torch.ones(numNodesOuter*numSamples) * 0.0).view(numSamples,numNodesOuter,1)  # outer bias is a variable in this case; initial values set to 0.0
    # # Constants (independent of learnable parameters)
    # StateInnerWeightMask = torch.ones(numNodesInner,numNodesOuter)
    # StateInnerWeightMask[0,0] = 0
    # StateInnerWeightMask[(numWeightsPerOuterCell-1),-1] = 0
    WeightsOuterMax = ParWeightsOuterMax.clone()
    return(W_inner1,B_inner1,W_inner2,B_inner2,B_outer,WeightsOuterMax)

def Evaluate(observed, target):
    [allStateOuter, allStateOuterTimeConstants, allStateInnerTimeConstants] = observed
    [TargetPattern, TargetStateOuterTimeConstants, TargetStateInnerTimeConstants] = target
    initEvalTimeStep = int(EvaluationProp*SimTimeSteps)
    obs1 = allStateOuter[:,:,-initEvalTimeStep:]
    distance1 = (obs1 - TargetPattern.unsqueeze(2)).pow(2).mean()  # shapes: obs = (numSamples,numNodesOuter,SimTimeSteps+1); target = (numNodesOuter,1)
    obs2 = allStateOuterTimeConstants[-initEvalTimeStep:]
    obs2 = obs2 / 2  # scale it so that the distances are comparable and no one distance dominates
    distance2 = (obs2 - TargetStateOuterTimeConstants.unsqueeze(0)).pow(2).mean()
    obs3 = allStateInnerTimeConstants[-initEvalTimeStep:]
    obs3 = obs3 / 2  # scale it so that the distances are comparable and no one distance dominates
    distance3 = (obs3 - TargetStateInnerTimeConstants.unsqueeze(0)).pow(2).mean()
    # distance = ((2/3) * distance1) + ((1/3) * (distance2 + distance3))
    distance = (distance1 + distance2 + distance3) / 3
    return(distance)

def Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                 W_outer,B_outer,StateOuter_timeConstants,StateInner_timeConstants1,StateInner_timeConstants2,numSamples):
    # torch.set_grad_enabled(False)  # for truncated BPTT
    StateOuter = StateOuter.unsqueeze(2)  # shape = (numSamples,numNodesOuter,1) to match with W_outer shape = (numSamples,numNodesOuter,numNodesOuter)
    allStateOuter = torch.FloatTensor(np.repeat(-1,numNodesOuter*numSamples)).view(StateOuter.shape)  # shape = (numSamples,numNodesOuter,SimTimeSteps)
    allStateInnerIn1 = (torch.ones(StateInnerIn1.shape)*-1).unsqueeze(0)  # shape = (SimTimeSteps,numSamples,numInNodesController1,numNodesOuter)
    allStateInnerIn2 = (torch.ones(StateInnerIn2.shape)*-1).unsqueeze(0)  # shape = (SimTimeSteps,numSamples,numInNodesController2,numNodesOuter)
    allWeightOuter = torch.FloatTensor(np.repeat(-1,numWeightsOuter*numSamples)).view(numWeightsOuter,numSamples)
    allWeightOuter = allWeightOuter.unsqueeze(0)  # shape = (SimTimeSteps,numWeightsOuter,numSamples)
    allBiasOuter = torch.FloatTensor(np.repeat(-1,numNodesOuter*numSamples)).view(B_outer.shape)  # shape = (numSamples,numNodesOuter,SimTimeSteps)
    allStateOuterTimeConstants = torch.zeros(SimTimeSteps+1,*StateOuter_timeConstants.shape)
    allStateInnerTimeConstants1 = torch.zeros(SimTimeSteps+1,*StateInner_timeConstants1.shape)
    allStateInnerTimeConstants2 = torch.zeros(SimTimeSteps+1,*StateInner_timeConstants2.shape)
    # append to all* variables along the dimension corresponding to SimTimeSteps
    allStateOuter = torch.cat((allStateOuter,StateOuter),dim=2)
    allStateInnerIn1 = torch.cat((allStateInnerIn1,StateInnerIn1.unsqueeze(0)),dim=0)
    allStateInnerIn2 = torch.cat((allStateInnerIn2,StateInnerIn2.unsqueeze(0)),dim=0)
    allStateOuterTimeConstants[0] = StateOuter_timeConstants
    allStateInnerTimeConstants1[0] = StateInner_timeConstants1
    allStateInnerTimeConstants2[0] = StateInner_timeConstants2
    x, y = np.where(torch.triu(AdjOuter,1)==1)  # does not include self-loops
    initWeightsOuter = W_outer[:,x,y].view(numWeightsOuterNonSelfEdges,numSamples)  # ordering is correct
    initWeightsOuterSelfLoops = torch.diagonal(W_outer,offset=0,dim1=1,dim2=2).t().view(numWeightsOuterSelfLoops, numSamples)
    initWeightsOuter = torch.cat((initWeightsOuter,initWeightsOuterSelfLoops),dim=0)  # shape = (numWeightsOuter,numSamples)
    allWeightOuter = torch.cat((allWeightOuter, initWeightsOuter.unsqueeze(0)), dim=0)  # shape = (SimTimeSteps,numWeightsOuter,numSamples)
    allBiasOuter = torch.cat((allBiasOuter, B_outer), dim=2)
    for s in range(SimTimeSteps):
        StateOuterNextDtSigmoid = torch.sigmoid(StateOuter - B_outer)
        StateOuterNextDtSigmoidWeighted = torch.bmm(W_outer,StateOuterNextDtSigmoid)
        StateOuterNextDt = -StateOuter + StateOuterNextDtSigmoidWeighted
        StateOuter = StateOuter + (StateOuterNextDt * timestep)
        if Stochastic:
            MeanStateOuter = StateOuter.mean().abs().data
            StateOuter = StateOuter + torch.normal(mean=0.0, std=torch.FloatTensor([0.05 * MeanStateOuter]))
        StateOuter = StateOuter / StateOuter_timeConstants.unsqueeze(2)
        # # TEST: Perturb state
        # if (s>=0) and (s<=1950):
        #     StateOuter = StateOuter + torch.normal(0,20,StateOuter.size())
        # Controller 1
        StateInnerOutSigmoid1 = torch.sigmoid(StateInnerOut1 - B_inner1)
        StateInnerInNextSigmoidWeighted1 = torch.matmul(W_inner1, StateInnerOutSigmoid1)  # NOTE: Possible source of memory leak
        StateInnerInNextDt1 = -StateInnerIn1 + StateInnerInNextSigmoidWeighted1
        StateInnerIn1 = StateInnerIn1 + (StateInnerInNextDt1 * timestep)
        StateInnerIn1 = StateInnerIn1 / StateInner_timeConstants1
        # # TEST: Perturb state
        # if (s>=0) and (s<=1950):
        #     StateInnerIn1 = StateInnerIn1 + torch.normal(0,20,StateInnerIn1.size())
        # Note that the outer bias will be contained within [-1,1], which is fine as long as the outer states are within the same range and transformed through a standard sigmoid with slope 1 (hard-coding)
        # Order of variables (rows) in StaterInnerIn1: left weight, bias, self weight, right weight and the rest of the controller nodes
        if CellType == 'DynamicCellType':
            B_outer = StateInnerIn1[:,1,:].view(numSamples,numNodesOuter,1)  # outer bias index = 1 (hard-coding)
        WeightsOuter = StateInnerIn1[:,[0,numWeightsPerOuterCell],:]  # outer weight indices = [0, numWeightsPerOuterCell] (hard-coding)
        WeightsOuter = WeightsOuter.view(numSamples,1,-1)
        WeightsOuterTrim = WeightsOuter[:, :, 1:-1].view(numSamples,2,-1)  # trim the trailing (boundary) zeros for now; num outer weights per cell for chain = 2 (hard-coding)
        WeightsOuterTrim = WeightsOuterTrim.mean(dim=1)  # column-wise sum
        WeightsOuterTrim = WeightsOuterTrim * WeightsOuterMax
        W_outer = torch.triu(AdjOuter,1)  # does not include diagonal
        W_outer = W_outer.repeat(numSamples,1,1)  # shape = (numSamples,numNodesOuter,numNodesOuter)
        x,y = np.where(W_outer[0,:,:]==1)
        W_outer[:,x,y] = torch.FloatTensor(WeightsOuterTrim)  # ordering is correct
        W_outer = W_outer + W_outer.transpose(1,2)
        # include self-loop weights
        WeightsSelfLoop = StateInnerIn1[:,2,:].view(numSamples, numNodesOuter)  # self-loop weight index = 2 (hard-coding)
        WeightsSelfLoop = WeightsSelfLoop * WeightsOuterMax
        W_outer_diag = torch.diag_embed(WeightsSelfLoop)  # ordering is correct: every row of WeightsSelfLoop is a diagonal W_outer_diag w.r.t. dims 1 and 2
        W_outer = W_outer + W_outer_diag
        # NOTE: StateInnerOut and StateIStateInnerIn[:,[0,numWeightsPerOuterCell],:] have the states of Layer2NodesController in common (because layer2 nodes are both 'in' and 'out')
        StateInnerOut1 = StateInnerIn1[:,numLayer1NodesController1In:,]  # shape = (numSamples,numLayer1NodesController1Bidir+numLayer2NodesController1,numNodesOuter)
        StateInnerOut1 = torch.cat((StateOuter.transpose(1,2),StateInnerOut1),dim=1)  # append StateOuter since it's an 'out' node
        WeightsOuterIncSelfLoops = torch.cat((WeightsOuterTrim,WeightsSelfLoop),dim=1)
        WeightsOuterIncSelfLoops = WeightsOuterIncSelfLoops.transpose(0,1)
        # Controller 2
        StateInnerOutSigmoid2 = torch.sigmoid(StateInnerOut2 - B_inner2)
        StateInnerInNextSigmoidWeighted2 = torch.matmul(W_inner2, StateInnerOutSigmoid2)  # NOTE: Possible source of memory leak
        StateInnerInNextDt2 = -StateInnerIn2 + StateInnerInNextSigmoidWeighted2
        StateInnerIn2 = StateInnerIn2 + (StateInnerInNextDt2 * timestep)
        StateInnerIn2 = StateInnerIn2 / StateInner_timeConstants2
        # Compute timeconstant change
        # Order of variables (rows) in StaterInnerIn2: left tau, self tau, right tau and the rest of the controller nodes
        timeConstantTimeConstantsLeftRightDt = StateInnerIn2[:,[0,2],:]  # left and right timeconstants indices = 0,2 (hard-coding)
        timeConstantTimeConstantsLeftRightDt = torch.cat((torch.zeros(numSamples,2,1),timeConstantTimeConstantsLeftRightDt,torch.zeros(numSamples,2,1)),2).view(numSamples,1,-1)
        timeConstantTimeConstantsLeftRightDt = timeConstantTimeConstantsLeftRightDt[:,:,2:-2].view(numSamples,2,-1) # 2:-2 because the timeconstant of a cell is determined by cells on the left and right that are not adjacent cells (unlike for outer weights)
        timeConstantTimeConstantsSelfDt = StateInnerIn2[:,[1],:]  # self timeconstants indices = 1 (hard-coding)
        timeConstantTimeConstantsDt = torch.cat((timeConstantTimeConstantsLeftRightDt, timeConstantTimeConstantsSelfDt), dim=1)
        timeConstantTimeConstantsDt = timeConstantTimeConstantsDt.mean(dim=1)  # column-wise sum; shape = (numSamples,numNodesOuter)
        # Update timeconstants: this is where the 'timecontant change' is broadcasted to all the other timeconstants; since the timeconstant of timeconstant is always 1 they won't change
        StateOuter_timeConstants = StateOuter_timeConstants + (timeConstantTimeConstantsDt * timestep)  # StateOuter shape = (numSamples,numNodesOuter)
        StateInner_timeConstants1 = StateInner_timeConstants1 + (timeConstantTimeConstantsDt.unsqueeze(1) * timestep)  #StateInner1 shape = (numSamples,numInNodesController1,numNodesOuter)
        StateInner_timeConstants2 = StateInner_timeConstants2 + (timeConstantTimeConstantsDt.unsqueeze(1) * timestep)  #StateInner2 shape = (numSamples,numInNodesController2,numNodesOuter)
        StateInnerOut2 = StateInnerIn2[:,numLayer1NodesController2In:,]  # shape = (numSamples,numLayer1NodesController2Bidir+numLayer2NodesController2,numNodesOuter)
        TimeConstants = StateInner_timeConstants2[:,0,:].unsqueeze(1)  # shape = (numSamples,1,numNodesOuter)
        StateInnerOut2 = torch.cat((TimeConstants, StateInnerOut2), dim=1)  # append tau since it's an 'out' node
        StateInnerOut2 = torch.cat((StateOuter.transpose(1,2),StateInnerOut2),dim=1)  # append StateOuter since it's an 'out' node
        allStateOuter = torch.cat((allStateOuter,StateOuter),dim=2)
        allWeightOuter = torch.cat((allWeightOuter,WeightsOuterIncSelfLoops.unsqueeze(0)),dim=0)  # Note: StateInner[0,1:] = StateInner[1,:-1]
        allBiasOuter = torch.cat((allBiasOuter,B_outer),dim=2)
        allStateInnerIn1 = torch.cat((allStateInnerIn1, StateInnerIn1.unsqueeze(0)), dim=0)
        allStateInnerIn2 = torch.cat((allStateInnerIn2, StateInnerIn2.unsqueeze(0)), dim=0)
        allStateOuterTimeConstants[s+1] = StateOuter_timeConstants  # this is not a storage-shared copy
        allStateInnerTimeConstants1[s+1] = StateInner_timeConstants1  # this is not a storage-shared copy
        allStateInnerTimeConstants2[s+1] = StateInner_timeConstants2  # this is not a storage-shared copy
    allStateOuter = allStateOuter[:,:,1:]
    allWeightOuter = allWeightOuter[1:,:,:]
    allBiasOuter = allBiasOuter[:,:,1:]
    allStateInnerIn1 = allStateInnerIn1[1:,:,:,:]
    allStateInnerIn2 = allStateInnerIn2[1:,:,:,:]
    return(allStateOuter,allWeightOuter,allBiasOuter,allStateInnerIn1,allStateInnerIn2,StateOuter,StateInnerIn1,StateInnerIn2,
           StateInnerOut1,StateInnerOut2,B_outer,W_outer,allStateOuterTimeConstants,allStateInnerTimeConstants1,allStateInnerTimeConstants2)

## Specify simulation parameters
# Mode = 'Learn'  # Options: Simulate, Learn
timestep = 0.01
if (Mode == 'Simulate') or (Mode == 'SimulatePhase1Only'):
    MinTotalLoss, BestParams = torch.load('./Data/BestParamsLocal_DynamicSize_MultipleControllers_' + str(FileNumber) + '.dat')
    CellType = 'DynamicCellType'  # this must be saved in BestParams
    if CellType == 'DynamicCellType':
        ParWeightsInner1, ParWeightsInner2, ParBiasInner1, ParBiasInner2, \
        AdjOuter, AdjInner1, AdjInner2, \
        LayeringController1InterDirections, LayeringController2InterDirections, \
        LayeringController1, LayeringController2, ParWeightsOuterMax, \
        ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
    else:
        ParWeightsInner, ParBiasInner, ParBiasOuter, \
        AdjOuter, AdjInner, \
        LayeringControllerInterDirections, LayeringController, ParWeightsOuterMax, \
        ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
    numStopCells = 2
    numNodesOuter = AdjOuter.size()[0]
    # Update AdjOuter if simulating with a different network size
    if numCells != (numNodesOuter - numStopCells):  # numCells is supplied as an argument
        numNodesOuter = numCells + numStopCells  # one 'stop' cell at each pole
        LayeringOuter = [numNodesOuter, 1]  # outer layer nodes
        AdjOuter = computeLatticeAdjacencyMatrix(LayeringOuter)  # numpy matrix; self-loops are added separately below
        # Add self-loops to outer cells
        cellIndices = list(range(numNodesOuter))
        AdjOuter[cellIndices, cellIndices] = 1
        AdjOuter = torch.FloatTensor(AdjOuter)
    numWeightsOuterNonSelfEdges = int(np.sum(np.triu(AdjOuter, 1)))  # edges not including self-loops
    numWeightsOuterSelfLoops = int(np.sum(np.diag(AdjOuter)))  # self-loops only
    numWeightsOuter = numWeightsOuterNonSelfEdges + numWeightsOuterSelfLoops
    numWeightsPerOuterCell = int(torch.sum(AdjOuter[1,:]))  # in-degree of cell; same for all cells with indices 1 through (n-2) assuming homogeneous connectivity; Note that cells 0 and (n-1) have a smaller degree than the rest; includes self-loops
    numWeightsPerOuterCellSelfLoop = int(AdjOuter[1,1].item())  # assuming all cells are connected in the same way (e.g., either all cells have self-loops or they don't)
    numWeightsPerOuterCellNonSelfEdges = numWeightsPerOuterCell - numWeightsPerOuterCellSelfLoop
    # Controller1 variables
    numLayer1NodesController1In = len([x for x in LayeringController1InterDirections if x == 'in'])
    numLayer1NodesController1Out = len([x for x in LayeringController1InterDirections if x == 'out'])
    numLayer1NodesController1Bidir = len([x for x in LayeringController1InterDirections if x == 'bidirectional'])
    numLayer1NodesController1 = numLayer1NodesController1In + numLayer1NodesController1Out + numLayer1NodesController1Bidir
    cells_per_layer_ctrl1 = [np.prod(x) for x in LayeringController1]
    numLayer2NodesController1 = cells_per_layer_ctrl1[1]
    numInNodesController1 = numLayer1NodesController1In + numLayer1NodesController1Bidir + numLayer2NodesController1
    numOutNodesController1 = numLayer1NodesController1Out + numLayer1NodesController1Bidir + numLayer2NodesController1
    numAdjInnerCells1 = numLayer1NodesController1 + numLayer2NodesController1
    # Controller2 variables
    numLayer1NodesController2In = len([x for x in LayeringController2InterDirections if x == 'in'])
    numLayer1NodesController2Out = len([x for x in LayeringController2InterDirections if x == 'out'])
    numLayer1NodesController2Bidir = len([x for x in LayeringController2InterDirections if x == 'bidirectional'])
    numLayer1NodesController2 = numLayer1NodesController2In + numLayer1NodesController2Out + numLayer1NodesController2Bidir
    cells_per_layer_ctrl2 = [np.prod(x) for x in LayeringController2]
    numLayer2NodesController2 = cells_per_layer_ctrl2[1]
    numInNodesController2 = numLayer1NodesController2In + numLayer1NodesController2Bidir + numLayer2NodesController2
    numOutNodesController2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir + numLayer2NodesController2
    numAdjInnerCells2 = numLayer1NodesController2 + numLayer2NodesController2
    LearnIters = 1
    torch.set_grad_enabled(False)
elif (Mode == 'Learn') or (Mode == 'LearnPhase1Only'):
    CellType = 'DynamicCellType'  # options: HeterogeneousCellType, HomogeneousCellType, DynamicCellType
    numStopCells = 2
    if ResumeLearning:
        ResumeFileName = './Data/BestParamsLocal_DynamicSize_MultipleControllers_' + str(FileNumber) + '.dat'  # same as SaveFileName
        _, BestParams = torch.load(ResumeFileName)
        # if CellType == 'DynamicCellType':
        #     ParWeightsInner, ParBiasInner, \
        #     AdjOuter, AdjInner, \
        #     LayeringControllerInterDirections, LayeringController, ParWeightsOuterMax, \
        #     ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
        # else:
        #     ParWeightsInner, ParBiasInner, ParBiasOuter, \
        #     AdjOuter, AdjInner, \
        #     LayeringControllerInterDirections, LayeringController, ParWeightsOuterMax, \
        #     ParWeightTimeConstantUpperBound, ParStateTimeConstantUpperBound = BestParams
        # numNodesOuter = AdjOuter.size()[0]
        # numCells = numNodesOuter - numStopCells
        # numWeightsPerOuterCell = int(AdjOuter[1,:].sum())  # in-degree of cell; same for all cells with indices 1 through (n-2) assuming homogeneous connectivity; Note that cells 0 and (n-1) have a smaller degree than the rest; includes self-loops
    else:
        numNodesOuter = numCells + numStopCells  # one 'stop' cell at each pole
        LayeringOuter = [numNodesOuter,1]  # outer layer nodes
        LayeringLayer2Controller1 = [3,3]
        LayeringLayer2Controller2 = [2,3]
        AdjOuter = computeLatticeAdjacencyMatrix(LayeringOuter)  # numpy matrix; self-loops are added separately below
        # Add self-loops to outer cells
        cellIndices = list(range(numNodesOuter))
        AdjOuter[cellIndices, cellIndices] = 1
        numWeightsPerOuterCell = int(np.sum(AdjOuter[1,:]))  # in-degree of cell; same for all cells with indices 1 through (n-2) assuming homogeneous connectivity; Note that cells 0 and (n-1) have a smaller degree than the rest; includes self-loops
        # NOTE: this program assumes that the directions follow the specific order: in, out, bidir
        # That is, all nodes with 'in' direction are listed first, then followed by 'out' and bidir' in that order.
        # The corresponding variables are: left weight, bias, self weight, right weight and outer state.
        LayeringController1InterDirections = ['in', 'in', 'in', 'in', 'out']  # indicate the directions of the arrow connecting layer 1 nodes and layer 2 wrt layer 1 nodes; length should be equal to the number of nodes in layer 1 of controller 1
        # The corresponding variables are: left tau, self tau, right tau, outer state and self tau.
        LayeringController2InterDirections = ['in', 'in', 'in', 'out', 'out']  # indicate the directions of the arrow connecting layer 1 nodes and layer 2 wrt layer 1 nodes; length should be equal to the number of nodes in layer 1 of controller 2
        # Controller1 variables
        numLayer1NodesController1In = len([x for x in LayeringController1InterDirections if x == 'in'])
        numLayer1NodesController1Out = len([x for x in LayeringController1InterDirections if x == 'out'])
        numLayer1NodesController1Bidir = len([x for x in LayeringController1InterDirections if x == 'bidirectional'])
        numLayer1NodesController1 = numLayer1NodesController1In + numLayer1NodesController1Out + numLayer1NodesController1Bidir  # numWeightsPerOuterCell + 1 + 1  # weight nodes of a single outer cell (3 for a chain, including self-loop), bias of a single outer cell (1), state of a single outer cell (1)
        LayeringController1 = [[numLayer1NodesController1,1],LayeringLayer2Controller1]  # the local controller1 of a single outer cell (3x3) that controllers the properties of the corresponding cell
        cells_per_layer_ctrl1 = [np.prod(x) for x in LayeringController1]
        numLayer2NodesController1 = cells_per_layer_ctrl1[1]
        numOutNodesController1 = numLayer1NodesController1Out + numLayer1NodesController1Bidir + numLayer2NodesController1
        # Controller2 variables
        numLayer1NodesController2In = len([x for x in LayeringController2InterDirections if x == 'in'])
        numLayer1NodesController2Out = len([x for x in LayeringController2InterDirections if x == 'out'])
        numLayer1NodesController2Bidir = len([x for x in LayeringController2InterDirections if x == 'bidirectional'])
        numLayer1NodesController2 = numLayer1NodesController2In + numLayer1NodesController2Out + numLayer1NodesController2Bidir  # 3 + 2  # left, self and right timeconstants (2); outer state and self tau (2)
        LayeringController2 = [[numLayer1NodesController2,1],LayeringLayer2Controller2]  # the local controller2 of a single outer cell (2x3) that controllers the life constants of the neighboring cells
        cells_per_layer_ctrl2 = [np.prod(x) for x in LayeringController2]
        numLayer2NodesController2 = cells_per_layer_ctrl2[1]
        numOutNodesController2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir + numLayer2NodesController2
        ## Create Weight matrices
        # Define learnable parameters (tensors with grads): W_u=[w_I,w_u]; B_u=[b_w,b_u]; B_l=[b_l]
        # Inter-connectivity in the controller could be asymmetric
        numAdjInnerCells1 = numLayer1NodesController1 + numLayer2NodesController1
        numAdjInnerCells2 = numLayer1NodesController2 + numLayer2NodesController2
        # CellIndicesPerLayer for controller 1
        AllCellIndices = list(range(numAdjInnerCells1))
        CellIndicesPerLayer1 = []
        Curr = 0
        for i in cells_per_layer_ctrl1:
            CellIndicesPerLayer1.append(AllCellIndices[Curr:(Curr + i)])
            Curr += i
        # CellIndicesPerLayer for controller 2
        AllCellIndices = list(range(numAdjInnerCells2))
        CellIndicesPerLayer2 = []
        Curr = 0
        for i in cells_per_layer_ctrl2:
            CellIndicesPerLayer2.append(AllCellIndices[Curr:(Curr + i)])
            Curr += i
        AdjOuter = torch.FloatTensor(AdjOuter)
        AdjInner1 = computeControllerAdjacencyMatrix(LayeringController1, numLayer1NodesController1In, numLayer1NodesController1Bidir,
                                                     numLayer2NodesController1, numLayer1NodesController1Out,
                                                     CellIndicesPerLayer1, ControllerType='GRN')
        AdjInner2 = computeControllerAdjacencyMatrix(LayeringController2, numLayer1NodesController2In, numLayer1NodesController2Bidir,
                                                     numLayer2NodesController2, numLayer1NodesController2Out,
                                                     CellIndicesPerLayer2, ControllerType='Life')  # life constants (tau)
        AdjInner1 = torch.FloatTensor(AdjInner1)  # directed network if LayeringControllerInterDirections has in and out connections
        AdjInner2 = torch.FloatTensor(AdjInner2)
        # numWeightsInter for controller 1
        numWeightsInter1 = 0
        for i in range(numLayer1NodesController1):  # this loop is essential in case AdjInner is directed/asymmetric
            if (LayeringController1InterDirections[i] == 'in') or (LayeringController1InterDirections[i] == 'bidirectional'):  # the condition following 'or' can also be clubbed with the 'out' condition below
                numWeightsInter1 += int(torch.sum(AdjInner1[i, :]))
            elif LayeringController1InterDirections[i] == 'out':
                numWeightsInter1 += int(torch.sum(AdjInner1[:, i - numLayer1NodesController1In]))
        # numWeightsInter for controller 2
        numWeightsInter2 = 0
        for i in range(numLayer1NodesController2):  # this loop is essential in case AdjInner is directed/asymmetric
            if (LayeringController2InterDirections[i] == 'in') or (LayeringController2InterDirections[i] == 'bidirectional'):  # the condition following 'or' can also be clubbed with the 'out' condition below
                numWeightsInter2 += int(torch.sum(AdjInner2[i, :]))
            elif LayeringController2InterDirections[i] == 'out':
                numWeightsInter2 += int(torch.sum(AdjInner2[:, i - numLayer1NodesController2In]))
        # Inner-connectivity for controller1 (presently assumed to be symmetric)
        start1 = numLayer1NodesController1In + numLayer1NodesController1Bidir
        end1 = start1 + numLayer2NodesController1
        start2 = numLayer1NodesController1Out + numLayer1NodesController1Bidir
        end2 = start2 + numLayer2NodesController1
        numWeightsInner1 = int(torch.sum(AdjInner1[start1:end1, start2:end2]) / 2)  # inner layer weights
        ParWeightsInner1 = torch.FloatTensor(np.random.uniform(-1,1,numWeightsInter1 + numWeightsInner1))  # weights of inner layer (inter-weights and lattice weights)
        ParBiasInner1 = torch.FloatTensor(np.random.uniform(-1,1,numOutNodesController1))  # bias of inner layer; only out nodes, including outer state, need a bias (c.f. CTRNN)
        # Inner-connectivity for controller2 (presently assumed to be symmetric)
        start1 = numLayer1NodesController2In + numLayer1NodesController2Bidir
        end1 = start1 + numLayer2NodesController2
        start2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir
        end2 = start2 + numLayer2NodesController2
        numWeightsInner2 = int(torch.sum(AdjInner2[start1:end1, start2:end2]) / 2)  # inner layer weights
        ParWeightsInner2 = torch.FloatTensor(np.random.uniform(-1,1,numWeightsInter2 + numWeightsInner2))  # weights of inner layer (inter-weights and lattice weights)
        ParBiasInner2 = torch.FloatTensor(np.random.uniform(-1,1,numOutNodesController2))  # bias of inner layer; only out nodes, including outer state, need a bias (c.f. CTRNN)
        if CellType == 'HeterogeneousCellType':
            ParBiasOuter = torch.FloatTensor(np.random.uniform(-1, 1, numNodesOuter))  # heterogeneous bias of outer layer
        elif CellType == 'HomogeneousCellType':
            ParBiasOuter = torch.FloatTensor(np.random.uniform(-1, 1, 1))  # homogeneous bias of outer layer
        elif CellType == 'DynamicCellType':
            pass  # Outer bias is a variable, not a parameter
        ParWeightTimeConstantUpperBound = torch.FloatTensor(np.random.uniform(1, 2, 1))
        ParStateTimeConstantUpperBound = torch.FloatTensor(np.random.uniform(1, 2, 1))
        ParWeightsOuterMax = torch.FloatTensor(np.random.uniform(1, 2, 1))  # outer weights range = [-WeightsOuterMax, WeightsOuterMax]
    numWeightsOuterNonSelfEdges = int(np.sum(np.triu(AdjOuter, 1)))  # edges not including self-loops
    numWeightsOuterSelfLoops = int(np.sum(np.diag(AdjOuter)))  # self-loops only
    numWeightsOuter = numWeightsOuterNonSelfEdges + numWeightsOuterSelfLoops
    numWeightsPerOuterCellSelfLoop = AdjOuter[1, 1]  # assuming all cells are connected in the same way (e.g., either all cells have self-loops or they don't)
    numWeightsPerOuterCellNonSelfEdges = numWeightsPerOuterCell - numWeightsPerOuterCellSelfLoop
    assert len(LayeringController1InterDirections) == numLayer1NodesController1
    assert len(LayeringController2InterDirections) == numLayer1NodesController2
    numLayer1NodesController1In = len([x for x in LayeringController1InterDirections if x == 'in'])
    numLayer1NodesController1Out = len([x for x in LayeringController1InterDirections if x == 'out'])
    numLayer1NodesController1Bidir = len([x for x in LayeringController1InterDirections if x == 'bidirectional'])
    numLayer1NodesController2In = len([x for x in LayeringController2InterDirections if x == 'in'])
    numLayer1NodesController2Out = len([x for x in LayeringController2InterDirections if x == 'out'])
    numLayer1NodesController2Bidir = len([x for x in LayeringController2InterDirections if x == 'bidirectional'])
    ## Create Adjacency matrices
    # Connectivity:
    # (1) a outer layer consisting of a chain of cells; and
    # (2) an inner layer consisting of: (2.1) a lattice of cells and (2.2) weights from a single outer layer cell connected to that lattice
    # CellIndicesPerLayer for controller 1
    AllCellIndices = list(range(numAdjInnerCells1))
    CellIndicesPerLayer1 = []
    Curr = 0
    for i in cells_per_layer_ctrl1:
        CellIndicesPerLayer1.append(AllCellIndices[Curr:(Curr + i)])
        Curr += i
    # CellIndicesPerLayer for controller 2
    AllCellIndices = list(range(numAdjInnerCells2))
    CellIndicesPerLayer2 = []
    Curr = 0
    for i in cells_per_layer_ctrl2:
        CellIndicesPerLayer2.append(AllCellIndices[Curr:(Curr + i)])
        Curr += i
    numLayer2NodesController1 = cells_per_layer_ctrl1[1]
    numLayer2NodesController2 = cells_per_layer_ctrl2[1]
    numAdjInnerCells1 = numLayer1NodesController1 + numLayer2NodesController1
    numAdjInnerCells2 = numLayer1NodesController2 + numLayer2NodesController2
    numOutNodesController1 = numLayer1NodesController1Out + numLayer1NodesController1Bidir + numLayer2NodesController1
    numInNodesController1 = numLayer1NodesController1In + numLayer1NodesController1Bidir + numLayer2NodesController1
    numOutNodesController2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir + numLayer2NodesController2
    numInNodesController2 = numLayer1NodesController2In + numLayer1NodesController2Bidir + numLayer2NodesController2
    ## Create Weight matrices
    # Define learnable parameters (tensors with grads): W_u=[w_I,w_u]; B_u=[b_w,b_u]; B_l=[b_l]
    # Inter-connectivity in the controller could be asymmetric
    # numWeightsInter for controller 1
    numWeightsInter1 = 0
    for i in range(numLayer1NodesController1):  # this loop is essential in case AdjInner is directed/asymmetric
        if (LayeringController1InterDirections[i] == 'in') or (LayeringController1InterDirections[i] == 'bidirectional'):  # the condition following 'or' can also be clubbed with the 'out' condition below
            numWeightsInter1 += int(torch.sum(AdjInner1[i, :]))
        elif LayeringController1InterDirections[i] == 'out':
            numWeightsInter1 += int(torch.sum(AdjInner1[:, i - numLayer1NodesController1In]))
    # numWeightsInter for controller 2
    numWeightsInter2 = 0
    for i in range(numLayer1NodesController2):  # this loop is essential in case AdjInner is directed/asymmetric
        if (LayeringController2InterDirections[i] == 'in') or (LayeringController2InterDirections[i] == 'bidirectional'):  # the condition following 'or' can also be clubbed with the 'out' condition below
            numWeightsInter2 += int(torch.sum(AdjInner2[i, :]))
        elif LayeringController2InterDirections[i] == 'out':
            numWeightsInter2 += int(torch.sum(AdjInner2[:, i - numLayer1NodesController2In]))
    # Inner-connectivity in controller1 (presently assumed to be symmetric)
    start1 = numLayer1NodesController1In + numLayer1NodesController1Bidir
    end1 = start1 + numLayer2NodesController1
    start2 = numLayer1NodesController1Out + numLayer1NodesController1Bidir
    end2 = start2 + numLayer2NodesController1
    numWeightsInner1 = int(torch.sum(AdjInner1[start1:end1,start2:end2])/2)  # inner layer weights
    # Inner-connectivity in controller2 (presently assumed to be symmetric)
    start1 = numLayer1NodesController2In + numLayer1NodesController2Bidir
    end1 = start1 + numLayer2NodesController2
    start2 = numLayer1NodesController2Out + numLayer1NodesController2Bidir
    end2 = start2 + numLayer2NodesController2
    numWeightsInner2 = int(torch.sum(AdjInner2[start1:end1,start2:end2])/2)  # inner layer weights
    torch.set_grad_enabled(True)

TargetPattern = torch.FloatTensor(np.linspace(1,-1,numNodesOuter-numStopCells))
TargetPattern = torch.cat((torch.FloatTensor([0]),TargetPattern,torch.FloatTensor([0]))).view(1,numNodesOuter)  # padding 0 for stop cells
targetLowerTimeConstant = 1  # (hard-coding)
TargetStateOuterTimeConstants = torch.ones(numInitStates,numNodesOuter) * targetLowerTimeConstant
TargetStateOuterTimeConstants[:,[0,-1]] = targetUpperTimeConstant  # stop cells
TargetStateInnerTimeConstants = torch.ones(numInitStates,numInNodesController1,numNodesOuter)  # WHY DOES THIS DEPEND ON numInNodesController?
TargetStateInnerTimeConstants[:,:,[0,-1]] = targetUpperTimeConstant   # stop cells
MinTotalLoss = torch.FloatTensor([math.inf])
MaxLearnStasisToleranceLong = 500  # 500
MaxLearnStasisToleranceShort = 100  # 100
LearnStasisCounterLong = 0
LearnStasisCounterShort = 0
SaveFileName = './Data/BestParamsLocal_DynamicSize_MultipleControllers_' + str(FileNumber) + '.dat'

for learniter in range(LearnIters):

    torch.set_grad_enabled(True)
    ParWeightsInner1.requires_grad = True
    ParBiasInner1.requires_grad = True
    ParWeightsInner2.requires_grad = True
    ParBiasInner2.requires_grad = True
    ParWeightTimeConstantUpperBound.requires_grad = True
    ParStateTimeConstantUpperBound.requires_grad = True
    ParWeightsOuterMax.requires_grad = True
    if CellType != 'DynamicCellType':
        ParBiasOuter.requires_grad = True

    TotalParWeightsInnerGrad1 = torch.zeros(ParWeightsInner1.size())
    TotalParBiasInnerGrad1 = torch.zeros(ParBiasInner1.size())
    TotalParWeightsInnerGrad2 = torch.zeros(ParWeightsInner2.size())
    TotalParBiasInnerGrad2 = torch.zeros(ParBiasInner2.size())
    if CellType != 'DynamicCellType':
        TotalParBiasOuterGrad = torch.zeros(ParBiasOuter.size())
    TotalParWeightTimeConstantUpperBound = torch.zeros(ParWeightTimeConstantUpperBound.size())
    TotalParStateTimeConstantUpperBound = torch.zeros(ParStateTimeConstantUpperBound.size())
    TotalParWeightsOuterMax = torch.zeros(ParWeightsOuterMax.size())

    # Define simulation parameters; these are not directly learnable
    W_inner1, B_inner1, W_inner2, B_inner2, B_outer, WeightsOuterMax = InstantiateSimulationParametersConstants(numInitStates)  # Note that W_outer (WeightsOuter) is a variable, not a parameter

    ## Simulation routine for state updates with sigmoid transformation of Weight matrices
    # Simulation: initial conditions for states (S_u=[s_u,w_l]; S_l=[s_l]) and weights (w_l)
    # Two phases:
    # 1) start with a few weights severed and wait until both weights and pattern are generated and stabilized; and
    # 2) repeat step 1 but severing the weights again but disturbing nothing else, and wait until weights and pattern are regenerated and restabilized

    ## Phase 1
    # Initial conditions for outer and inner states
    # if numInitStates > 1:
    #     StateOuter1 = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter-numStopCells), np.linspace(0.1, 2, numInitStates))))  # shape = (numInitStates,numNodesOuter); multiple initial conditions (gradients to make learning easier, analogous to "organizer" in developmental biology)
    # elif numInitStates == 1:
    #     StateOuter1 = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter-numStopCells), np.linspace(1, 1, numInitStates))))  # shape = (1,numNodesOuter)
    # The same initial outer state regardless of numInitStates since it was found that they hardly influence the outcome
    # Also, numInitStates will dictate the number of initial alive cells (see below)
    ## Initial outer state contains a gradient in the middle
    StateOuter = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(1, 1, numInitStates))))  # shape = (numInitStates,numNodesOuter)
    # # TEST: random initial outer state
    # StateOuter = torch.FloatTensor(np.random.uniform(-1, 1, 10)).view(numInitStates,numNodesOuter - numStopCells)
    # # TEST: initial outer state with all 0s
    # StateOuter = torch.FloatTensor(list(map(lambda x: np.linspace(x, -x, numNodesOuter - numStopCells), np.linspace(0, 0, numInitStates))))  # shape = (numInitStates,numNodesOuter)
    StateOuter = torch.cat((torch.tensor([0]*numInitStates).view(-1, 1), StateOuter, torch.tensor([0]*numInitStates).view(-1, 1)), dim=1)  # padding values for stop cells
    StateInnerOut1 = torch.zeros(numInitStates,numOutNodesController1,numNodesOuter)
    StateInnerOut2 = torch.zeros(numInitStates, numOutNodesController2, numNodesOuter)
    # Initial condition of outer state is part of the inner state, since the state of each cell feeds into the cell's "GRN"
    # This state will be referred to as 'StateInnerOut' since it contains all variables with out and bidir links;
    # while the state that contains variables with only in and bidir links will be referred to as 'StateInnerIn'.
    StateInnerOut1[:,0,:] = StateOuter.detach()  # clone is not necessary here; detach will work
    StateInnerOut2[:, 0, :] = StateOuter.detach()  # clone is not necessary here; detach will work
    StateInnerIn1 = torch.zeros(numInitStates,numInNodesController1,numNodesOuter)
    StateInnerIn2 = torch.zeros(numInitStates, numInNodesController2, numNodesOuter)
    # NOTE: StateInnerOut and StateInnerIn have the states of Layer2NodesController in common (because layer2 nodes are both 'in' and 'out')
    # create outer weight matrix from the weight state vector
    W_outer = torch.triu(AdjOuter)
    x,y = np.where(W_outer==1)
    WeightsOuter1Flat = np.zeros(numWeightsOuter)  # initial outer weights are set to 0
    W_outer[x,y] = torch.FloatTensor(WeightsOuter1Flat)
    W_outer = W_outer + W_outer.t()
    W_outer = W_outer.repeat(numInitStates,1,1)  # shape = (numInitStates,numNodesOuter,numNodesOuter)
    # B_outer = B_outer.clone()  # shape = (numNodesOuter,numInitStates)
    StateOuter_timeConstants = torch.ones(numInitStates,numNodesOuter) * targetUpperTimeConstant   # all but the middle cells are dead initially # Not the same values as StateInnerIn1[:,-2:,], as the latter are timeconstant changes, not timeconstants themselves
    StateInner1_timeConstants = torch.ones(numInitStates,numInNodesController1,numNodesOuter) * targetUpperTimeConstant  # all but the middle cells are dead initially # same values as StateInnerIn1[:,-2:,] initially
    StateInner2_timeConstants = torch.ones(numInitStates,numInNodesController2,numNodesOuter) * targetUpperTimeConstant  # all but the middle cells are dead initially # same values as StateInnerIn1[:,-2:,] initially
    center = (numNodesOuter / 2) + 0.5  # median cell
    initOffset = 0.5 if numCells % 2 == 0 else 1
    cutOffsets = np.arange(initOffset, numInitStates + 0.5, 1)  # the length of this array should be equal to numInitStates = halfNumCells
    for sample in range(len(cutOffsets)):
        offset = cutOffsets[sample]
        st, nd = int(center - offset) - 1, int(center + offset)
        # # TEST: shift locations of initially alive cells
        # st -= 4; nd-= 4
        # # TEST: change the number of initially alive cells
        # st -= 2; nd+= 2
        # # TEST: random range of initially alive cells
        # st -= np.random.randint(0,int(numNodesOuter/2)); nd+= np.random.randint(0,int(numNodesOuter/2))
        # # TEST: all cells are initially alive (including the stop cells)
        # st = 0; nd = 11
        # only the alive cells have non-zero initial states; all variables except StateOuter are set to 0 regardless
        StateOuter[sample, 0:st] = 0
        StateOuter[sample, nd:] = 0
        # # TEST: random initial lower timeconstants
        # numAliveCells = nd - st
        # initTimeConstants = torch.FloatTensor(np.random.uniform(1, 2, numAliveCells))
        # all cells are dead
        # initTimeConstants = torch.FloatTensor(np.random.uniform(2, 2, numAliveCells))
        # StateOuter_timeConstants[sample, st:nd] = initTimeConstants  # only the middle cells are alive initially
        # StateInner1_timeConstants[sample, :, st:nd] = initTimeConstants  # only the middle cells are alive initially
        # StateInner2_timeConstants[sample, :, st:nd] = initTimeConstants
        StateOuter_timeConstants[sample, st:nd] = targetLowerTimeConstant  # only the middle cells are alive initially
        StateInner1_timeConstants[sample, :, st:nd] = targetLowerTimeConstant  # only the middle cells are alive initially
        StateInner2_timeConstants[sample, :, st:nd] = targetLowerTimeConstant  # only the middle cells are alive initially
    # simulate
    allStateOuter, allWeightOuter, allBiasOuter, allStateInnerIn1, allStateInnerIn2, StateOuter, \
    StateInnerIn1, StateInnerIn2, StateInnerOut1, StateInnerOut2, B_outer, W_outer, \
    allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2 = \
        Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
                 W_outer,B_outer,StateOuter_timeConstants,StateInner1_timeConstants,StateInner2_timeConstants,numInitStates)
    # # TEST: regenerate
    # StateOuter = StateOuter.squeeze(2)  # shape = (numSamples, numNodesOuter)
    # StateOuter_timeConstants = allStateOuterTimeConstants[-1]
    # StateInner1_timeConstants = allStateInnerTimeConstants1[-1]
    # StateInner2_timeConstants = allStateInnerTimeConstants2[-1]
    # for sample in range(len(cutOffsets)):
    #     offset = cutOffsets[sample]
    #     st, nd = int(center - offset) - 1, int(center + offset)
    #     # TEST: shift locations of initially alive cells
    #     # st += 4; nd+= 4
    #     StateOuter[sample, 0:st] = 0
    #     StateOuter[sample, nd:] = 0
    #     B_outer[sample, 0:st, :] = 0
    #     B_outer[sample, nd:, :] = 0
    #     W_outer[:, 0:st, 0:st] = 0
    #     W_outer[:, nd:, nd:] = 0
    #     StateInnerOut1[:, :, 0:st] = 0
    #     StateInnerOut1[:, :, nd:] = 0
    #     StateInnerOut2[:, :, 0:st] = 0
    #     StateInnerOut2[:, :, nd:] = 0
    #     StateInnerIn1[:, :, 0:st] = 0
    #     StateInnerIn1[:, :, nd:] = 0
    #     StateInnerIn2[:, :, 0:st] = 0
    #     StateInnerIn2[:, :, nd:] = 0
    #     StateOuter_timeConstants[sample, 0:st] = targetUpperTimeConstant
    #     StateOuter_timeConstants[sample, nd:] = targetUpperTimeConstant
    #     StateInner1_timeConstants[sample, :, 0:st] = targetUpperTimeConstant
    #     StateInner1_timeConstants[sample, :, nd:] = targetUpperTimeConstant
    #     StateInner2_timeConstants[sample, :, 0:st] = targetUpperTimeConstant
    #     StateInner2_timeConstants[sample, :, nd:] = targetUpperTimeConstant
    # # simulate
    # allStateOuter, allWeightOuter, allBiasOuter, allStateInnerIn1, allStateInnerIn2, StateOuter, \
    # StateInnerIn1, StateInnerIn2, StateInnerOut1, StateInnerOut2, B_outer, W_outer, \
    # allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2 = \
    #     Simulate(StateOuter,StateInnerOut1,StateInnerOut2,StateInnerIn1,StateInnerIn2,
    #              W_outer,B_outer,StateOuter_timeConstants,StateInner1_timeConstants,StateInner2_timeConstants,numInitStates)
    # compute loss
    observed = [allStateOuter,allStateOuterTimeConstants,allStateInnerTimeConstants1]
    target = [TargetPattern,TargetStateOuterTimeConstants,TargetStateInnerTimeConstants]
    Loss = Evaluate(observed,target)
    # Compute gradients
    if (Mode == 'Learn') or (Mode == 'LearnPhase1Only'):
        Loss.backward(retain_graph=True)
        lr1 = ComputeLearningRate(ParWeightsInner1,'weight')
        TotalParWeightsInnerGrad1 += lr1 * ParWeightsInner1.grad.data
        lr2 = ComputeLearningRate(ParWeightsInner2,'weight')
        TotalParWeightsInnerGrad2 += lr2 * ParWeightsInner2.grad.data
        lr3 = ComputeLearningRate(ParBiasInner1,'bias')
        TotalParBiasInnerGrad1 += lr3 * ParBiasInner1.grad.data
        lr4 = ComputeLearningRate(ParBiasInner2,'bias')
        TotalParBiasInnerGrad2 += lr4 * ParBiasInner2.grad.data
        if CellType != 'DynamicCellType':
            lr5 = ComputeLearningRate(ParBiasOuter,'bias')
            TotalParBiasOuterGrad += lr5 * ParBiasOuter.grad.data
        ParWeightsInner1.grad.data.zero_()
        ParWeightsInner2.grad.data.zero_()
        if CellType != 'DynamicCellType':
            ParBiasOuter.grad.data.zero_()
        ParBiasInner1.grad.data.zero_()
        ParBiasInner2.grad.data.zero_()
        lr6 = ComputeLearningRate(ParWeightsOuterMax, 'range')
        TotalParWeightsOuterMax += lr6 * ParWeightsOuterMax.grad.data
        ParWeightsOuterMax.grad.data.zero_()

    ## break the computational graph, so memory leak is stemmed
    torch.set_grad_enabled(False)
    ParWeightsInner1.requires_grad = False
    ParBiasInner1.requires_grad = False
    ParWeightsInner2.requires_grad = False
    ParBiasInner2.requires_grad = False
    ParWeightTimeConstantUpperBound.requires_grad = False
    ParStateTimeConstantUpperBound.requires_grad = False
    ParWeightsOuterMax.requires_grad = False
    if CellType != 'DynamicCellType':
        ParBiasOuter.requires_grad = False

    ## Save best parameters
    if (Mode == 'Learn') or (Mode == 'Simulate'):
        TotalLoss = Loss.data # + Loss2.data
    elif (Mode == 'LearnPhase1Only') or (Mode == 'SimulatePhase1Only'):
        TotalLoss = Loss.data
    if (Mode == 'Learn'):
        if TotalLoss <= MinTotalLoss:
            MinTotalLoss = TotalLoss.detach()
            ## Save parameters
            if CellType != 'DynamicCellType':
                BestParams = [ParWeightsInner1.data.clone(), ParWeightsInner2.data.clone(),
                              ParBiasInner1.data.clone(), ParBiasInner2.data.clone(), ParBiasOuter.data.clone(),
                              AdjOuter.clone(), AdjInner1.clone(), AdjInner2.clone(),
                              LayeringController1InterDirections, LayeringController2InterDirections,
                              LayeringController1, LayeringController2, ParWeightsOuterMax.clone(),
                              ParWeightTimeConstantUpperBound.clone(), ParStateTimeConstantUpperBound.clone()]
            else:
                BestParams = [ParWeightsInner1.data.clone(), ParWeightsInner2.data.clone(),
                              ParBiasInner1.data.clone(), ParBiasInner2.data.clone(),
                              AdjOuter.clone(), AdjInner1.clone(), AdjInner2.clone(),
                              LayeringController1InterDirections, LayeringController2InterDirections,
                              LayeringController1, LayeringController2, ParWeightsOuterMax.clone(),
                              ParWeightTimeConstantUpperBound.clone(), ParStateTimeConstantUpperBound.clone()]
            torch.save([MinTotalLoss, BestParams], SaveFileName)
            LearnStasisCounterShort = 0
            LearnStasisCounterLong = 0

    # Print results
    if Verbose:
        print(learniter, Loss.data, TotalLoss.data, MinTotalLoss, ParWeightsOuterMax.data)

    ## Update parameters
    if (Mode == 'Learn') or (Mode == 'LearnPhase1Only'):
        ParWeightsInner1.data -= TotalParWeightsInnerGrad1
        ParBiasInner1.data -= TotalParBiasInnerGrad1
        ParWeightsInner2.data -= TotalParWeightsInnerGrad2
        ParBiasInner2.data -= TotalParBiasInnerGrad2
        if CellType != 'DynamicCellType':
            ParBiasOuter.data -= TotalParBiasOuterGrad
        ParWeightTimeConstantUpperBound.data -= TotalParWeightTimeConstantUpperBound
        ParStateTimeConstantUpperBound.data -= TotalParStateTimeConstantUpperBound
        ParWeightTimeConstantUpperBound.data[ParWeightTimeConstantUpperBound.data < 0] = 0
        ParStateTimeConstantUpperBound.data[ParStateTimeConstantUpperBound.data < 0] = 0
        ParWeightsOuterMax.data -= TotalParWeightsOuterMax
        ParWeightsOuterMax.data[ParWeightsOuterMax.data < 1.0] = 1.0  # impose a minimum of 1.0 so that the smallest outer weight range is [-1,1]

    if (LearnStasisCounterLong == MaxLearnStasisToleranceLong) or (TotalLoss.item() == float('inf')) or (TotalLoss != TotalLoss):  # last condition checks for nan
        print('resetting params to random values')
        ParWeightsInner1.data = torch.FloatTensor(np.random.uniform(-1, 1, numWeightsInter1 + numWeightsInner1))
        ParBiasInner1.data = torch.FloatTensor(np.random.uniform(-1, 1, numOutNodesController1))
        ParWeightsInner2.data = torch.FloatTensor(np.random.uniform(-1, 1, numWeightsInter2 + numWeightsInner2))
        ParBiasInner2.data = torch.FloatTensor(np.random.uniform(-1, 1, numOutNodesController2))
        if CellType == 'HeterogeneousCellType':
            ParBiasOuter.data = torch.FloatTensor(np.random.uniform(-1, 1, numNodesOuter))
        elif CellType == 'HomogeneousCellType':
            ParBiasOuter.data = torch.FloatTensor(np.random.uniform(-1, 1, 1))
        elif CellType == 'DynamicCellType':
            pass  # Outer bias is a variable, not a parameter
        ParWeightTimeConstantUpperBound.data = torch.FloatTensor(np.random.uniform(1, 2, 1))
        ParStateTimeConstantUpperBound.data = torch.FloatTensor(np.random.uniform(1, 2, 1))
        ParWeightsOuterMax.data = torch.FloatTensor(np.random.uniform(1, 2, 1))
        LearnStasisCounterLong = 0
        LearnStasisCounterShort = 0
    elif (LearnStasisCounterShort == MaxLearnStasisToleranceShort):
        # reset params to previous best and add noise
        print('resetting params to close to previous best')
        ParWeightsInner1.data = BestParams[0].clone()
        ParWeightsInner2.data = BestParams[1].clone()
        ParBiasInner1.data = BestParams[2].clone()
        ParBiasInner2.data = BestParams[3].clone()
        ParWeightTimeConstantUpperBound.data = BestParams[-2].data.clone()
        ParStateTimeConstantUpperBound.data = BestParams[-1].data.clone()
        ParWeightsOuterMax.data = BestParams[-3].data.clone()
        ParWeightsInner1.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParWeightsInner1.size()[0]))
        ParBiasInner1.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParBiasInner1.size()[0]))
        ParWeightsInner2.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParWeightsInner2.size()[0]))
        ParBiasInner2.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParBiasInner2.size()[0]))
        if CellType != 'DynamicCellType':
            ParBiasOuter.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParBiasOuter.size()[0]))
        ParWeightTimeConstantUpperBound.data += torch.normal(mean=0.0, std=torch.FloatTensor(
            [0.01] * ParWeightTimeConstantUpperBound.size()[0]))
        ParStateTimeConstantUpperBound.data += torch.normal(mean=0.0, std=torch.FloatTensor(
            [0.01] * ParStateTimeConstantUpperBound.size()[0]))
        ParWeightsOuterMax.data += torch.normal(mean=0.0, std=torch.FloatTensor([0.01] * ParWeightsOuterMax.size()[0]))
        LearnStasisCounterShort = 0
    else:
        LearnStasisCounterLong += 1
        LearnStasisCounterShort += 1

if (Mode == 'Learn') or (Mode == 'LearnPhase1Only'):
    torch.save([MinTotalLoss,BestParams],SaveFileName)
elif Mode == 'Simulate':
    if CellType == 'DynamicCellType':
        torch.save([allStateOuter, allWeightOuter, allBiasOuter, AdjOuter,
                    allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2],'./Data/SimulationDataLocal_DynamicSize_MultipleControllers_' + str(FileNumber) + '.dat')
    else:
        torch.save([allStateOuter,allWeightOuter,AdjOuter],'./Data/SimulationDataLocal.dat')
elif Mode == 'SimulatePhase1Only':
    if CellType == 'DynamicCellType':
        torch.save([allStateOuter, allWeightOuter, allBiasOuter, AdjOuter,
                    allStateOuterTimeConstants, allStateInnerTimeConstants1, allStateInnerTimeConstants2],'./Data/SimulationDataLocal_DynamicSize_MultipleControllers.dat')
    else:
        torch.save([allStateOuter, allWeightOuter, AdjOuter],'./Data/SimulationDataLocal_DynamicSize_MultipleControllers.dat')

# import matplotlib.pyplot as plt
# plt.plot(allStateOuter1[0,:,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants1[:,0,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants1[:,0,:].detach().numpy());plt.show()
# plt.plot(allStateInnerTimeConstants1[:,0,:,5].detach().numpy());plt.show()
# plt.plot(allWeightOuter1[0:100,4,0].detach().numpy());plt.show()
# plt.plot(allStateOuter2[0,:,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuter2[1,:,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuter2[2,:,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,0,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,1,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,2,[0,-1]].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,0,:].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,1,:].detach().numpy());plt.show()
# plt.plot(allStateOuterTimeConstants2[:,2,:].detach().numpy());plt.show()
# plt.plot(allStateInnerTimeConstants2[:,0,:,5].detach().numpy());plt.show()












