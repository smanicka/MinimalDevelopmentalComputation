import torch

class Sensitivity():

    def __init__(self,Variables,Parameters):
        self.Variables = Variables
        self.Parameters = Parameters
        self.NumSamples = self.Variables.shape[0]
        self.NumVariables = self.Variables.shape[1]
        assert self.Parameters.shape[0] == self.NumSamples

    def ComputeGradients(self,save=False,saveFilename=None):
        SensitivityData = torch.zeros(self.NumVariables,self.NumSamples,*self.Parameters.shape[1:])  # we assume that dim 0 of Parameters represents the samples (hence the assert in init)
        for i in range(self.NumSamples):
            for j in range(self.NumVariables):
                self.Variables[i,j].backward(retain_graph=True,create_graph=False)  # create_graph=True for higher-order gradients
                SensitivityData[j,i] += self.Parameters.grad.data[i]  # d(vm_j_t_iter)/d(vm_cell_t_0) for input i
                self.Parameters.grad.data.zero_()  # IMPORTANT STEP: incorrect results if skipped
                if save:
                    torch.save(SensitivityData,saveFilename)
        return(SensitivityData)